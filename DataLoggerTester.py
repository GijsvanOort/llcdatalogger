# DataLoggerTester.py
from PySide import QtGui, QtCore

"""Small script that emulates the data logger part of the llc. (parts of it)"""

import socket
import threading
import sys
import struct
import time
import math
import queue
import numpy as np

_timeAtStartup = time.time()-4000
def TimeSinceStartup():
    return time.time() - _timeAtStartup

def rounded(v,sampleRate):
    """Returns v, but rounded to the nearest 1/sampleRate second."""
    return round(v * sampleRate) / sampleRate
class ConnectionLost(Exception):
    def __str__(self):
        return ("ConnecionLost exception")
    
class ClientThread(threading.Thread):
    """For each client connection, one thread of this is created."""
    SAMPLE_RATE = 1000 # Samples per second
    
    SYNC_ID                  = 0xDACD4368;
    DATALOGGER_COLUMN_COUNT  = 16;
    DATALOGGER_START                = 0;
    DATALOGGER_STOP                 = 1;
    DATALOGGER_CONFIGURE            = 2;
    DATALOGGER_PARAMETER_ADD        = 3;
    DATALOGGER_PARAMETER_REMOVE     = 4;
    DATALOGGER_PARAMETER_REMOVE_ALL = 5;
    DATALOGGER_STATUS               = 6;
    DATALOGGER_DATA                 = 7;
    DATALOGGER_ERROR                = 8;
    
    def __init__(self, ip, port, socket, printDebug):
        threading.Thread.__init__(self)
        self._ip = ip
        self._port = port
        self._clientSocket = socket
        self._clientSocket.settimeout(20)
        self._sockID = "#"+str(socket.fileno())+"#"
        print (self._sockID,"[+] New thread started for ",ip,":",str(port),". Socket: ", self._sockID)
        
        self._parameters = [] # List of all parameters which are logged.
        self._lastTimeRelative = 0 # Just make sure the variable exists
        # Add a queue for telling messages to the thread
        self._q = queue.Queue()
        self.printDebug = printDebug
    def run(self):
        """Serves. I.e., waits for a request from the client and responds to it."""
        while True:
            try:
                needToSkip = self._q.get_nowait()
                print(self._sockID,"Skipping %i samples"%needToSkip)
                self._lastTimeRelative = rounded(self._lastTimeRelative + needToSkip / self.SAMPLE_RATE, self.SAMPLE_RATE)
            except queue.Empty:
                # No request for skipping samples
                pass
                
            try:
                # Read until we receive something which looks like a header
                data = self._clientSocket.recv(16)
                if not data:
                    print(self._sockID," 0 bytes received. Connection was closed. Let's wait for a new one...")
                    raise ConnectionLost
                elif len(data)!= 16:
                    # A header is 16 bytes long, so this can't be a header
                    print(self._sockID,"Looking for a 16 byte header, but received a %d byte chunck"%len(data))
                    continue
                header = self._unpackInts(data)
                if header[0] != self.SYNC_ID:
                    print(self._sockID,"Looking for a header starting with the Sync ID, but received a 16 byte chunck which starts with something else.")
                    continue
                    
                # If we're here, we received a good header.
                if self.printDebug:
                    print (self._sockID,'received header: ',header)
                
                # Now we need to receve the payload: this should be header[2] bytes long.
                if header[2]!=0:
                    data = self._clientSocket.recv(header[2])
                    if len(data)!=header[2]:
                        print(self._sockID,"Expected a payload of %d bytes, but received %d bytes."%(header[2],len(data)))
                        continue
                    payload = data
                else:
                    payload = b''
                    
                # If we're here, we have received both header and payload. So, execute the function
                functions = [\
                    self._start,         # 0, start
                    self._stopCommand,   # 1, stop
                    self._configure,     # 2, configure
                    self._addParameter,  # 3, Parameter_Add
                    self._remove,        # 4, Parameter_remove
                    self._removeAll,     # 5, Parameter_remove_all
                    self._unimplemented, # 6, Status
                    self._data,          # 7, Data
                    self._unimplemented] # 8, Error
                
                functionToExecute = functions[header[1]]
                replyHeader, replyPayloadTuple = functionToExecute(header,payload)
                self._clientSocket.sendall(self._packInts(replyHeader))
                for replyPayload in replyPayloadTuple:
                    if len(replyPayload)>0:
                        self._clientSocket.sendall(replyPayload)
            except (socket.error, ConnectionLost) as e:
                print(self._sockID,"Exception:")
                print(self._sockID,e)
                print(self._sockID,"Ending this thread (don't bother about closing the connection properly...)")
                break
        
        # If we're here, we received an exception and we need to end execution (i.e., end the 'run' function)
            

    def _unpackInts(self,b):
        """Given big-endian bytes b, returns a tuple of ints."""
        n = len(b)//4
        return struct.unpack(">%dI"%n, b[0:n*4])
        
    def _packInts(self,i):
        """Returns a big-endian concatenation of ints"""
        return struct.pack(">%dI"%len(i), *i)
    
    def _packFloats(self,f):
        """Returns a big-endian concatenation of floats (4-byte floating point)"""
        return struct.pack(">"+"f"*len(f), *f)
   
    def _interleave(self, *lists):
        return [item for pair in zip(*lists) for item in pair]
        
    def _configure(self,header,payload):
        payload_int = self._unpackInts(payload)
        print(self._sockID,"Configure Packet. subSample=%d, flushSizeMax=%d."%payload_int)
        self._subSample, self._flushSizeMax = payload_int
        return (self.SYNC_ID, self.DATALOGGER_CONFIGURE,0,0), ()
  
    def _addParameter(self,header,payload):
        parameterName = payload.decode()
        self._parameters.append((parameterName,))
        print(self._sockID,'Parameter add; added parameter "%s".'%parameterName)
        return (self.SYNC_ID, self.DATALOGGER_PARAMETER_ADD,0,0), ()
        
    def _remove(self,header,payload):
        """I think this implements the remove_all function"""
        paramName = payload.decode()
        self._parameters = [p for p in self._parameters if p[0]!=paramName]
        print(self._sockID,"Remove Parameter packet. Removing parameter \"%s\""%paramName)
        return (self.SYNC_ID, self.DATALOGGER_PARAMETER_REMOVE,0,0), ()
    
    def _removeAll(self,header,payload):
        print(self._sockID,"Remove AllParameter packet. Removing all parameters.")
        self._parameters = []
        return (self.SYNC_ID, self.DATALOGGER_PARAMETER_REMOVE_ALL,0,0), ()
        
    def _start(self,header,payload):
        self._t0 = rounded(TimeSinceStartup(),self.SAMPLE_RATE) # Time at which the logging for this channel started
        self._lastTimeRelative = -1/self.SAMPLE_RATE # Contains the (relative) time of the last sample sent to the logger
     
        self._isRunning = True
        print(self._sockID,"Starting scope.")
        return (self.SYNC_ID, self.DATALOGGER_START,0,0), ()
    
    def _stopCommand(self,header,payload):
        self._isRunning = False
        print(self._sockID,"Stopping scope.")
        return (self.SYNC_ID, self.DATALOGGER_STOP,0,0), ()
        
    def _data(self,header,payload):
        """The most important function: requesting data..."""
        # Determine number of samples
        t_relative = rounded(TimeSinceStartup(),self.SAMPLE_RATE)-self._t0
        nSamples = math.floor ((t_relative-self._lastTimeRelative)*self.SAMPLE_RATE)
        if nSamples > self._flushSizeMax:
            nSamples = self._flushSizeMax

        # Create first payload: number of parameters and number of samples
        
        # Create the real payload
        if nSamples>0:
            payload1 = self._packInts( (len(self._parameters), nSamples) )
            # Generate sampletime (int) and globaltime 
            
            sampleTimes = [ self._lastTimeRelative + (k+1)/self.SAMPLE_RATE for k in range(nSamples) ]
            timesSinceStartup = [ t + self._t0 for t in sampleTimes ]
            sampleTimesRounded = [rounded(t,self.SAMPLE_RATE) for t in sampleTimes]
            timesSinceStartupRounded = [rounded(t, self.SAMPLE_RATE) for t in timesSinceStartup ]
            
            # Make all signals sine waves, except if they are sampleTime or #Lopes.TimeSinceStartup or #Lopes.TimeSinceStartupWrapped
            # Sine signals will be sine waves with a frequency of 0.2*[1,2,3,...] Hz
            if self._parameters[0][0] == "sampleTime":
                payload2 = sampleTimesRounded[:]
            else:
                f = 0.2 * 0.1 # A very low frequency
                payload2 = [math.sin(2*math.pi*f*t) for t in sampleTimesRounded]
            if len(self._parameters)>=2:
                if self._parameters[1][0] == "#Lopes.TimeSinceStartup":
                    payload2.extend(timesSinceStartupRounded)
                else:
                    f = 0.2 * 1 # Frequency in Hz
                    payload2.extend([math.sin(2*math.pi*f*t) for t in sampleTimesRounded]) # Take 0.1 Hz instead of 0 Hz

                if self._parameters[2][0] == "#Lopes.TimeSinceStartupWrapped":
                    payload2.extend([t%10 for t in timesSinceStartupRounded])
                else:
                    f = 0.2 * 2 # Frequency in Hz
                    payload2.extend([math.sin(2*math.pi*f*t) for t in sampleTimesRounded]) # Take 0.1 Hz instead of 0 Hz

                for i in range(3,len(self._parameters)):
                    f = 0.2*i # Frequency in Hz
                    payload2.extend( [math.sin(2*math.pi*f*t) for t in sampleTimesRounded])
                
            payload2bytes = self._packFloats(payload2)
            # Remember which was the most recent sample sent
            self._lastTimeRelative = sampleTimesRounded[-1]
        else:
            # Send no data
            nSamples = 0
            payload1 = self._packInts( (len(self._parameters), nSamples) )
            payload2bytes = b''
            
        if self.printDebug:
            print(self._sockID,"Data packet. Replying with %d samples of data (t_relative = %f, _lastTimeRelative = %f)."%(nSamples, t_relative, self._lastTimeRelative))
        return (self.SYNC_ID, self.DATALOGGER_DATA,0,0), (payload1,payload2bytes)
        
    def _unimplemented(self,header,payload):
        print(self._sockID,"Unimplemented header (function %d)"%header[1])
        return header,()
        
    def Skip(self,N):
        """You can call this function to force the ClientThread to skip
        N samples (or, if N is negative, re-send some). This can be used to
        test how the LLCDataLogger copes with this. The implementation is that
        a message is put onto a Queue, which is then read by the separate thread."""
        self._q.put (N)
        
class Server:
    """Server, accepting multiple connections"""
    def __init__(self, printDebugClient = True):
        self._clientThreads = []
        self._printDebugClient = printDebugClient
    def _setupServer(self):
        """Sets up the server"""
        # Create a TCP/IP socket
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        # Bind the socket to the port
        server_address = ('localhost', 4321)
        print ('starting up on %s port %s' % server_address)
        self._sock.bind(server_address)
        self._sock.settimeout(1)
        
        # Listen for incoming connections
        self._sock.listen(5)
        
    def Serve(self):
        self._setupServer()
        
        i=0
        while True:
            i=i+1 

            # Wait for a connection
            if len(self._clientThreads)==0:
                # Only print if there are no connections (otherwise, it clutters the log too much
                print ('waiting for a connection...')
            try:
                (client_sock, (client_address,client_port)) = self._sock.accept()
                newThread = ClientThread(client_address,client_port,client_sock,self._printDebugClient)
                self._clientThreads.append(newThread)
                newThread.start()
                
            except socket.timeout as e:
                if len(self._clientThreads)==0:
                    print('timed out', i)
            # Check clientThreads; if any is 'finished', remove it from the list of threads
            self._clientThreads = [c for c in self._clientThreads if c.isAlive()]
            
## Small GUI to influence functionality
class ButtonContainer(QtGui.QWidget):
    def __init__(self, theServer):
        QtGui.QWidget.__init__(self)
        self._server = theServer
        
        self._P = QtGui.QPushButton("Skip 10 from channel 0")
        self._P.clicked.connect(lambda: self._server._clientThreads[0].Skip(10))
        self._P.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self._P.setGeometry(QtCore.QRect(2104, 42, 145, 23))
        
        self._Q = QtGui.QPushButton("Redo 10 from channel 0")
        self._Q.clicked.connect(lambda: self._server._clientThreads[0].Skip(-10))
        self._Q.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self._Q.setGeometry(QtCore.QRect(2304, 42, 145, 23))

        self._l = QtGui.QVBoxLayout()
        self._l.addWidget(self._P)
        self._l.addWidget(self._Q)
        self.setLayout(self._l)
        self.setWindowTitle('DataLoggerTester')
        self.setMinimumWidth(225)
        self.show()
            
## 'Main program'   
app = QtGui.QApplication([])    
A = Server(False)
t = threading.Thread(target=A.Serve)
t.start()

B = ButtonContainer(A)
app.exec_()



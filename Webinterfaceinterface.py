# Webinterfaceinterface

"""This file implements a functions to talk to the LLC Webinterface. Only use it for quick testing!
For the moment, we can only set things, not read anything."""
import re
import urllib.parse, urllib.request
import time

# <form action="http://10.30.203.225/goform/formStats?page=view&address=Lopes.HapticRenderer&tab=Create" method="post" name="myform">

class WebInterfaceInterface:
    def __init__(self,url=None, debugPrint = False):
        if url is None:
            self._llcURL = 'http://10.30.203.225/goform/formStats'
        else:
            self._llcURL = url
        self._debugPrint = debugPrint
            
    def _MakeVector(self,pre,v,firstElement=0):
        """Given a 10 dof vector V, having floats and None's, it returns an urlencoded
        strink, like  Max1=&Max2=2.3,..."""
        if len(v) is not 10:
            raise RuntimeError('vector is not 10D')
        s=[]
        for i in range(10):
            num = str(v[i]) if v[i] is not None else ''
            s.append('%s%d=%s'%(pre,(i+firstElement),num))
        return '&'.join(s)

    def _Post(self,pageTab,params):
        """Posts the data.
        pageTab should be a tuple containing a page name and a 
        tab (=subpage)name, e.g. ('Lopes.HapticRenderer','Create')"""
        queryVariables = {'page':'view', 'address': pageTab[0], 'tab':pageTab[1]}
        fullURL = self._llcURL+'?'+urllib.parse.urlencode(queryVariables)
        if isinstance(params,str):
            if 'post=' not in params:
                params +='&post=apply'
            data = params.encode()
        else:
            if 'post' not in params.keys():
                params['post']='apply'
            data = urllib.parse.urlencode(params).encode()
        
        u=urllib.request.urlopen(fullURL,data)
        y=u.read().decode()
        if (self._debugPrint):
            print(y)
        u.close()
    
    def _Get(self, params=None):
        """Gets data. Params should be a dict"""
        if params is None:
            params = []
            fullURL = self._llcURL+'?'
        elif isinstance(params,list):
            # This ensures that the order of the parameters is fixed in the URL (needed for the WebInterfaceMirror)
            paramsAsString = '&'.join( [urllib.parse.urlencode({key:value}) for (key,value) in params] )
            fullURL = self._llcURL+'?'+paramsAsString
        else:    
            fullURL = self._llcURL+'?'+urllib.parse.urlencode(params)
        u = urllib.request.urlopen(fullURL)
        y=u.read().decode()
        if (self._debugPrint):
            print(y)
        u.close()
        return y
        
    def _GetParameterInHTMLString(self,s,paramName):
        """Returns the value of the given paramName."""
        findString= "<font class='font'>"+paramName+"</font>"
        index = s.find(findString)
        
        if index==-1:
            raise RuntimeError("Could not find parameter \"%s\" in webinterface page"%paramName)
            
        # We have now found the line in which the paramter name sits.
        # The value is four lines below. Let's find that...
        nextFewLines= s[index:].split('\n')
        valueLine = nextFewLines[4]
        
        # Let's find the value with a regex
        yyy=re.findall(">(.*)</a>",valueLine)
        if len(yyy)==1:
            return float(yyy[0])
        else:
            # Then the value was not clickable and the value is simply
            # the full line. (stripped)
            return valueLine.strip()
    
    def GetParams(self,paramNames, pageName, tab=None):
        """Returns the values of the parameters found on page pageName. If any of the parameters is not found,
        a runtime error will be raised. paramNames may be either a string (then this function returns a single value)
        or a list of strings (then it will return a list of values"""
        if isinstance(paramNames,str):
            paramNamesWasString = True
            paramNames = [paramNames]
        else:
            paramNamesWasString = False
            
        if tab is None:
            htmlString = self._Get([('page','view'),('address',pageName)])
        else:
            htmlString = self._Get([('page','view'),('address',pageName), ('tab',tab)])
        output = []
        for p in paramNames:
            output.append(self._GetParameterInHTMLString(htmlString,p))
        
        if paramNamesWasString:
            return output[0]
        else:
            return output
            
    def CreateSegmentDamper(self,name):
        pageTab = ('Lopes.HapticRenderer','Create')
        params = {
            'bias_force': '',
            'shaker':  '',
            'segmentspring': '',
            'damper': name,
            'post': 'apply'}
        self._Post(pageTab,params)
        
    def SetSegmentDamperParameters(self,name, viscous=None, coulomb=None, maxForce=None,enable=None):
        address = 'Lopes.HapticRenderer.'+name
        
        if viscous is not None:
            self._Post((address,'Viscous'),self._MakeVector('Damping ',viscous))
            time.sleep(0.1)
        if coulomb is not None:
            self._Post((address,'Coulomb'),self._MakeVector('Coulomb ',coulomb))
            time.sleep(0.1)
        if maxForce is not None:
            self._Post((address,'MaxForce'),self._MakeVector('Max Effect Force ',maxForce,1))
            time.sleep(0.1)
        if enable is not None:
            self._Post((address,'Effect'),{'Enabled':'true' if enable else 'false'})
            time.sleep(0.1)
        
    def SetCartesianInertia(self, inertia):
        """Given an inertia vector of 10 elements, it sets it, using the patient-inertia."""
        pageTab = ('Lopes.Model.Patient','CartesianInertia')
        paramNames = ['PELVIS X (inertia)','PELVIS Z (inertia)','LEFT KNEE X (inertia)',
            'LEFT ANKLE X (inertia)','LEFT ANKLE Z (inertia)','LEFT HEEL X (inertia)',
            'RIGHT KNEE X (inertia)','RIGHT ANKLE X (inertia)','RIGHT ANKLE Z (inertia)',
            'RIGHT HEEL X (inertia)']
        
        params = dict()
        for k,v in zip(paramNames, inertia):
            params[k]=v
        self._Post(pageTab, params)
        
    def UseCartesianInertia(self):
        """Sets the current inertia model to Cartesian Inertia"""
        self._Post(('Lopes.Model','TuningSet'),{'Use_Tuning_Set_Free':'', 'Use_Tuning_Set_Patient':'trigger','post':'start'})
        
    def LoadConfigFile(self):
        """Makes the LLC load the config file.
        http://10.30.203.225/goform/formStats?page=manager&action=loadConfig&awnser=yes
        """
        self._Get({'page':'manager', 'action':'loadConfig', 'awnser':'yes'})
        
    def SaveConfigFile(self):
        """Makes the LLC load the config file.
        http://10.30.203.225/goform/formStats?page=manager&action=loadConfig&awnser=yes
        """
        self._Get({'page':'manager', 'action':'saveConfig', 'awnser':'yes'})
        
    def ResetIterations(self):
        """Does a reset iterations."""
        self._Post(('Lopes.Transform','State'),{'Request':'-2'})
        time.sleep(1.5)
        self._Post(('Lopes.Transform','State'),{'Request':'-1'})
        
    def GetCurrentMassModel(self):
        """Returns the type of mass model used as a string:
        'SegmentMass','CartesianMass' or 'FullMass'"""
        s=self.GetParams("current frame","Lopes.Model.Inertia")
        return s
        
    def GetSegmentInertia(self, AlsoAsDict=False):
        """Returns the segmentinertia values as a list of values.
        If AlsoAsDict is True, this function returns a second argument,
        being the values as a dict with descriptive keys."""
        paramNames = ['pelvis tX (inertia)','pelvis tZ (inertia)', \
            'left thigh rX (inertia)','left thigh rZ (inertia)', \
            'left shank rZ (inertia)','left foot rZ (inertia)', \
            'right thigh rX (inertia)','right thigh rZ (inertia)', \
            'right shank rZ (inertia)','right foot rZ (inertia)']
        paramNamesForDict = ['PelvisTX','PelvisTZ',\
            'LeftThighRX','LeftThighRZ', 'LeftShankRZ','LeftFootRZ',\
            'RightThighRX','RightThighRZ', 'RightShankRZ','RightFootRZ']
        v = self.GetParams(paramNames,'Lopes.Model.Inertia','SegmentInertia')
        
        if not AlsoAsDict:
            return v
        else:
            vAsDict = dict(zip(paramNamesForDict,v))
            return v, vAsDict
            
    def GetCartesianInertia(self, AlsoAsDict = False):
        """Returns the cartesianinertia values as a list of values.
        If AlsoAsDict is True, this function returns a second argument,
        being the values as a dict with descriptive keys."""
        paramNames = ['PELVIS X (inertia)', 'PELVIS Z (inertia)', \
            'LEFT KNEE X (inertia)', 'LEFT ANKLE X (inertia)', \
            'LEFT ANKLE Z (inertia)', 'LEFT HEEL X (inertia)', \
            'RIGHT KNEE X (inertia)', 'RIGHT ANKLE X (inertia)', \
            'RIGHT ANKLE Z (inertia)', 'RIGHT HEEL X (inertia)']
        paramNamesForDict = ['PelvisX','PelvisZ',\
            'LeftKneeX','LeftAnkleX','LeftAnkleZ','LeftHeelX', \
            'RightKneeX','RightAnkleX','RightAnkleZ','RightHeelX']
        v = self.GetParams(paramNames,'Lopes.Model.Inertia','CartesianInertia')
        if not AlsoAsDict:
            return v
        else:
            vAsDict = dict(zip(paramNamesForDict,v))
            return v, vAsDict        

    def GetSegmentInertiaScaling(self, AlsoAsDict = False):
        """Returns the inertia scaling values for segment mass model
        as a list of values.
        If AlsoAsDict is True, this function returns a second argument,
        being the values as a dict with descriptive keys."""
        paramNames = ['pelvis tX alpha Left', 'pelvis tZ alpha Left', \
            'left thigh rX alpha Left', 'left thigh rZ alpha Left', \
            'left shank rZ alpha Left', 'left foot rZ alpha Left', \
            'right thigh rX alpha Left', 'right thigh rZ alpha Left', \
            'right shank rZ alpha Left', 'right foot rZ alpha Left', \
            'pelvis tX alpha right', 'pelvis tZ alpha right', \
            'left thigh rX alpha right', 'left thigh rZ alpha right', \
            'left shank rZ alpha right', 'left foot rZ alpha right', \
            'right thigh rX alpha right', 'right thigh rZ alpha right', \
            'right shank rZ alpha right', 'right foot rZ alpha right']
        paramNamesForDict = ['PelvisTX_Left', 'PelvisTZ_Left', \
            'LeftThighRX_Left', 'LeftThighRZ_Left', \
            'LeftShankRZ_Left', 'LeftFootRZ_Left', \
            'RightThighRX_Left', 'RightThighRZ_Left', \
            'RightShankRZ_Left', 'RightFootRZ_Left', \
            'PelvisTX_Right', 'PelvisTZ_Right', \
            'LeftThighRX_Right', 'LeftThighRZ_Right', \
            'LeftShankRZ_Right', 'LeftFootRZ_Right', \
            'RightThighRX_Right', 'RightThighRZ_Right', \
            'RightShankRZ_Right', 'RightFootRZ_Right']
        
        v = self.GetParams(paramNames,'Lopes.FootContact','SegmentInertiaScaling')
        if not AlsoAsDict:
            return v
        else:
            vAsDict = dict(zip(paramNamesForDict,v))
            return v, vAsDict    
            
    def GetCartesianInertiaScaling(self, AlsoAsDict = False):
        """Returns the inertia scaling values for cartesian mass model
        as a list of values.
        If AlsoAsDict is True, this function returns a second argument,
        being the values as a dict with descriptive keys."""
        paramNames = ['PELVIS X alpha Left', 'PELVIS Z alpha Left', \
            'LEFT KNEE X alpha Left', 'LEFT ANKLE X alpha Left', \
            'LEFT ANKLE Z alpha Left', 'LEFT HEEL X alpha Left', \
            'RIGHT KNEE X alpha Left', 'RIGHT ANKLE X alpha Left', \
            'RIGHT ANKLE Z alpha Left', 'RIGHT HEEL X alpha Left', \
            'PELVIS X alpha right', 'PELVIS Z alpha right', \
            'LEFT KNEE X alpha right', 'LEFT ANKLE X alpha right', \
            'LEFT ANKLE Z alpha right', 'LEFT HEEL X alpha right', \
            'RIGHT KNEE X alpha right', 'RIGHT ANKLE X alpha right', \
            'RIGHT ANKLE Z alpha right', 'RIGHT HEEL X alpha right']
        paramNamesForDict = ['PelvisX_Left', 'PelvisZ_Left', \
            'LeftKneeX_Left', 'LeftAnkleX_Left', \
            'LeftAnkleZ_Left', 'LeftHeelX_Left', \
            'RightKneeX_Left', 'RightAnkleX_Left', \
            'RightAnkleZ_Left', 'RightHeelX_Left', \
            'PelvisX_Right', 'PelvisZ_Right', \
            'LeftKneeX_Right', 'LeftAnkleX_Right', \
            'LeftAnkleZ_Right', 'LeftHeelX_Right', \
            'RightKneeX_Right', 'RightAnkleX_Right', \
            'RightAnkleZ_Right', 'RightHeelX_Right']
        
        v = self.GetParams(paramNames,'Lopes.FootContact','CartesianInertiaScaling')
        if not AlsoAsDict:
            return v
        else:
            vAsDict = dict(zip(paramNamesForDict,v))
            return v, vAsDict  
            
    def GetAccelerationRef(self, AlsoAsDict = False):
        """Returns the global AccelerationReference gain and the AccelerationRef's. Note that these
        are also parsable via the UDP API. Also, note that, normally the AccelerationRef's are
        constantly updated.
        If AlsoAsDict is True, this function returns a second argument,
        being the values as a dict with descriptive keys."""
        paramNames = ['GlobalAccRefGain', 'pelvis tX (acc Ref)', \
            'pelvis tZ (acc Ref)', 'left thigh rX (acc Ref)', \
            'left thigh rZ (acc Ref)', 'left shank rZ (acc Ref)', \
            'left foot rZ (acc Ref)', 'right thigh rX (acc Ref)', \
            'right thigh rZ (acc Ref)', 'right shank rZ (acc Ref)', \
            'right foot rZ (acc Ref)']
        paramNamesForDict = ['GlobalAccRefGain', 'PelvisTx',\
            'PelvisTz', 'LeftThighRx',\
            'LeftThighRz', 'LeftShankRz',\
            'LeftFootRz', 'RightThighRx',\
            'RightThighRz', 'RightShankRz',\
            'RightFootRz']
        v = self.GetParams(paramNames,'Lopes.Model','AccelerationRef')
        if not AlsoAsDict:
            return v
        else:
            vAsDict = dict(zip(paramNamesForDict,v))
            return v, vAsDict  
            
## Test script            
if __name__ == '__main__':
    # Test it with the real web inteface
#     W = WebInterfaceInterface(None,True)
    # or test it on the local mirror of the Webinterface:
    W = WebInterfaceInterface("http://127.0.0.1:8080/goform/formStats",False) # For use with the local mirror
    
#     W.CreateSegmentDamper('abcde')
#     W.SetSegmentDamperParameters('abcde',viscous=[1,2,3,4,None,6,7,8,9,10],coulomb=[1,2,3,4,None,6,7,8,9,10],enable=True)
#     W.SetCartesianInertia([10,20,30,400,500,600,700,800,900,100])
    print(W.GetCurrentMassModel())
    print(W.GetCartesianInertia())

    
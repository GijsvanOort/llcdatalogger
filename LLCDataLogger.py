# LLCDataLogger.py
## TOP
"""This script implements a simple GUI with which you can log LLC signals to disk.
It uses the same interface to the LLC as the JPlotter does.
The format used for writing the data to the disk is compatible with the .signals files
of the Lopes GUI."""
from PySide import QtCore, QtGui
import itertools
import socket
import struct
import datetime
import numpy as np
import os
import re
import time
import pyzolib.ssdf as ssdf
import visvis as vv
from ChannelSetDefinitions import ChannelSets
from LopesUDPInterface import LopesUDPInterface
from Webinterfaceinterface import WebInterfaceInterface
from WavRecorder import WavRecorder
from ping import ping

## Import (my own) Graph
# And fall back to a dummy implementation if pyqtgraph is not installed
try:
    from Graph import Graph
    pyqtgraphAvailable = True
except ImportError as E:
    print("Warning: pyqtgraph not found. Real-time graphs will not work.")
    pyqtgraphAvailable = False
    # Make a dummy class
    class Graph:
        def __init__(self,*args,**kwargs):
            pass
        def show(self):
            pass
        def AddOneDataPoint(self,point):
            pass
        def FinishUpdate(self):
            pass
        def deleteLater(self):
            pass

nan = float('nan')

## Configuration
defaultConfiguration = { # This one is used if the configuration file was not found
      # The real LLC
      'LLC_address': '10.30.203.225',
      'Logger_port': 4321,
      'WebInterface_port': 80,
      
      # Localhost for testing
#       'LLC_address': '127.0.0.1',
#       'Logger_port': 4321,
#       'WebInterface_port': 8080,
      
      'sampleFrequency': 1024,
      'subSample': 1, # decimation of samples (if 1, we receive 1024 samples/s; if 2, we receive 512 etc...
      'flushSizeMax': 1024, # Maximum number of samples sent in one packet
      
      'file_prefix': 'd:\\LLC_log_', # Where the files are stored plus the prefix for the name
      'file_extension': '.signals'
    }


## Functions
def flatten(l):
    """Flattens list l (one level)"""
    y=[]
    for x in l:
        y.extend(x)
    return y
    
class QDialogWithTextEdit(QtGui.QDialog):
    """A QDialog with a text-edit window. By default, it only had an Ok button."""
    def __init__(self, parent, title,text, buttons = QtGui.QDialogButtonBox.Ok):
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(title)
        self._vb = QtGui.QVBoxLayout()
        self._ed = QtGui.QTextEdit()
        self._ed.setPlainText(text)
        self._vb.addWidget(self._ed)
        self._pb = QtGui.QDialogButtonBox(buttons)
        self._pb.accepted.connect(self.accept)
        self._pb.rejected.connect(self.reject)
        self._vb.addWidget(self._pb)
        self.setLayout(self._vb)
        self.setSizeGripEnabled(True)
    
class LoggerConnection(QtCore.QObject):
    """This class implements the communication with the logger"""
    A=QtCore.Signal(str)
    DEBUG_SIGNAL = QtCore.Signal(str)
    COMMUNICATION_ERROR_SIGNAL = QtCore.Signal(str) # Is emitted when the connection has an error (e.g. timeout)
    
    SYNC_ID                  = 0xDACD4368
    DATALOGGER_START                = 0
    DATALOGGER_STOP                 = 1
    DATALOGGER_CONFIGURE            = 2
    DATALOGGER_PARAMETER_ADD        = 3
    DATALOGGER_PARAMETER_REMOVE     = 4
    DATALOGGER_PARAMETER_REMOVE_ALL = 5
    DATALOGGER_STATUS               = 6
    DATALOGGER_DATA                 = 7
    DATALOGGER_ERROR                = 8

    def __init__(self, configuration, debugFcn=None, connectionErrorFcn=None, name='unnamed'):
        """name should be a string which serves as an identifier in debug information"""
        QtCore.QObject.__init__(self)
        if debugFcn is not None:
            self.DEBUG_SIGNAL.connect(debugFcn)
        if connectionErrorFcn is not None:
            self.COMMUNICATION_ERROR_SIGNAL.connect(connectionErrorFcn)
        self._name = name
        self._namePrefix = self._name+': '
        self._llc_address = configuration['LLC_address']
        self._llc_port = configuration['Logger_port']
        self._subSample = configuration['subSample']
        self._flushSizeMax = configuration['flushSizeMax']
        self._isConnected = False
        
        self._channels = [] # Local copy of list of all channels
        self._dataBuffer = b''
        
        # Create a TCP/IP socket
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.settimeout(1)

        
    def _debug(self,s):
        """Sends the string s to the debug output"""
        self.DEBUG_SIGNAL.emit(self._namePrefix+s)
        
    def _recv(self,nBytes):
        """wrapper around self._sock.recv which allows keyboard interrupts. It also makes
        sure that we read exactly nBytes (the _sock.recv is repeated until we have enough bytes)"""
        if self._isConnected == False:
            print(self._namePrefix+"Warning: Call to _recv() while not connected (maybe disconnected because of an error)")
            return b''
        t0=time.time()
        y=b''
        i=0
        while time.time()-t0 <1.0:
            try:
                i=i+1
                y= y+ self._sock.recv(nBytes - len(y))
                if len(y)==nBytes:
                    return y
                time.sleep(0.1)
            except (socket.timeout,Exception, ConnectionAbortedError) as e:
                # This except catching makes Python ignore the timeout exception
                # and continue (i.e., trying again) to receive. Any other
                # exception will be raised up and will break the while loop.
                self._isConnected = False
                print(self._namePrefix+'self._recv() timed out or other exception:', e.args[0])
                self.COMMUNICATION_ERROR_SIGNAL.emit(e.args[0])
                return y
                
        # If we're here, the thing timed out
        self._isConnected = False
        errorstr='self._recv(): not enough bytes received (expected %d but received %d in %d tries)'%(nBytes,len(y),i)
        print(self._namePrefix+errorstr)
        self.COMMUNICATION_ERROR_SIGNAL.emit(self._namePrefix+errorstr)
        return y
                
    def _unpackInts(self,b):
        """Given big-endian bytes b, returns a tuple of ints."""
        n = len(b)//4
        return struct.unpack(">%dI"%n, b[0:n*4])

    def _unpackFloats(self,b):
        """Given big-endian bytes b, returns a tuple of 4-byte floats (single precision)."""
        n = len(b)//4
        return struct.unpack(">%df"%n, b[0:n*4])
        
    def _packInts(self,i):
        """Returns a big-endian concatenation of ints"""
        return struct.pack(">%dI"%len(i), *i)
    
    def _packFloats(self,f):
        """Returns a big-endian concatenation of floats (4-byte floating point)"""
        return struct.pack(">%df"%len(f), *f)
        
    def _readUntilEmpty(self):
        """Reads from the socket until no more data arrives. Basically, it
        clears the input buffer."""
        if not self._isConnected:
            print(self._namePrefix+"Warning: call to _readUntilEmpty() without being connected (possibly disconnected due to an error)")
            return 
            
        self._sock.settimeout(0)
        try:
            while True:
                reply= self._sock.recv(4096)
                if len(reply)==0:
                    break
        except BlockingIOError:
            pass
        except (Exception, ConnectionAbortedError) as e:
            self._isConnected = False
            self.COMMUNICATION_ERROR_SIGNAL.emit(e.args[0])
            return
                
        self._sock.settimeout(1)
        
    def ConnectToServer(self):
        """Connects to the ip address (self._llc_address)"""
        self.DisconnectFromServer()
        
        self._debug("Trying to connect to (%s, %d)"%(self._llc_address, self._llc_port))
        while True:
            try:
                self._sock.connect( (self._llc_address, self._llc_port) )
                break
            except socket.timeout as e:
                self._debug("Timeout. Trying again...")
        
        self._debug("Connected!")
        self._isConnected = True

    def DisconnectFromServer(self):
        if self._isConnected:
            self._debug("Closing existing connection.")
            self._sock.close()
            self._isConnected = False
            
    def _assertHeader(self, firstInts):
        """Waits until a response header (4 ints of 4 bytes each) is received. Then, returns
        when firstInts are identical to the first bytes of the response. If not, it raises a
        RuntimeError exception.
        This function returns the header (as ints) so that the caller can use it for further
        analysis (or ignore it))"""
        if not self._isConnected:
            print(self._namePrefix+"Warning: call to _assertHeader() without being connected (possibly disconnected due to an error");
            return
            
        reply= self._recv(16)
        
        if len(reply) !=16:
            raise RuntimeError(self._namePrefix+"Expected a reply header of 16 bytes but received a chunck of %d bytes."%len(reply))
        replyInts = self._unpackInts(reply)
        if replyInts[0:len(firstInts)]!=firstInts:
            print(self._namePrefix+"Expected a reply header starting with the following bytes: %s, but received this reply header: %s"%(firstInts, replyInts))
            #raise RuntimeError("Expected a reply header starting with the following bytes: %s, but received this reply header: %s"%(firstInts, replyInts))
            
    def Configure(self):
        """Sends a Configure packet to the server (and waits for its response).
        The data is obtained from self._subSample and self._flushSizeMax."""
        if not self._isConnected:
            raise RuntimeError(self._namePrefix+"No connection.")
            
        self._debug("Sending Configure packet, subSample=%d, flushSizeMax=%d."%(self._subSample, self._flushSizeMax))
        
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_CONFIGURE, 2*4, 0) )
        payload = self._packInts ( (self._subSample, self._flushSizeMax) )
        
        self._readUntilEmpty()
        self._sendall(header)
        self._sendall(payload)
            
        self._assertHeader( (self.SYNC_ID, self.DATALOGGER_CONFIGURE) )
        payload_int = self._unpackInts(payload)
        print(self._namePrefix+"Configure Packet. subSample=%d, flushSizeMax=%d."%payload_int)
        self._subSample, self._flushSizeMax = payload_int
        return (self.SYNC_ID, self.DATALOGGER_CONFIGURE,0,0), ()
        
    def AddChannel(self,name):
        """Adds the channel to the already existing list on the server."""
        if not self._isConnected:
            raise RuntimeError(self._namePrefix+"No connection.")
            
        self._debug("Sending AddChannel packet, channel: \"%s\""%name)
        name_enc = name.encode()
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_PARAMETER_ADD, len(name_enc), 0) )
        
        self._readUntilEmpty()
        self._sock.sendall(header)
        self._sock.sendall(name_enc)
        self._assertHeader( (self.SYNC_ID, self.DATALOGGER_PARAMETER_ADD) )
        
        self._channels.append(name)
    def RemoveChannel(self,name):
        """Removes the channel from the already existing list on the server."""
        if not self._isConnected:
            raise RuntimeError(self._namePrefix+"No connection.")
            
        self._debug("Sending RemoveChannel packet, channel: \"%s\""%name)
        name_enc = name.encode()
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_PARAMETER_REMOVE, len(name_enc), 0) )
        
        self._readUntilEmpty()
        self._sock.sendall(header)
        self._sock.sendall(name_enc)
        self._assertHeader( (self.SYNC_ID, self.DATALOGGER_PARAMETER_REMOVE) )
        
        self._channels.remove(name)
        
    def RemoveAllChannels(self):
        self._debug("Sending RemoveAllChannels packet")
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_PARAMETER_REMOVE_ALL, 0, 0) )
        
        self._readUntilEmpty()
        self._sock.sendall(header)
        self._assertHeader( (self.SYNC_ID, self.DATALOGGER_PARAMETER_REMOVE_ALL) )
        
        self._channels=[]
        
    def Start(self):
        """Sends a start-logging-request to the server. Also stores the index of the #Lopes.TimeSinceStartup channel (if any)"""
        try:
            self._TimeSinceStartupIndex = self._channels.index("#Lopes.TimeSinceStartup")
        except ValueError:
            self._TimeSinceStartupIndex = None
            
        self._debug("Sending Start packet")
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_START, 0, 0) )
        
        self._readUntilEmpty()
        self._sock.sendall(header)
        self._assertHeader( (self.SYNC_ID, self.DATALOGGER_START) )

    def Stop(self):
        """Sends a stop-logging-request to the server."""
        self._debug("Sending Stop packet")
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_STOP, 0, 0) )
        
        self._readUntilEmpty()
        self._sendall(header)
        self._assertHeader( (self.SYNC_ID, self.DATALOGGER_STOP) )
        
    def _sendall(self, s):
        """Wrapper around _sock.sendall which takes care of error handling"""
        if not self._isConnected:
            print(self._namePrefix+"Warning: call to _sendall() without being connected (possibly disconnected due to an error)")
            return 
            
        try:
            self._sock.sendall(s)
        except (Exception, ConnectionAbortedError) as e:
            self._isConnected = False
            self.COMMUNICATION_ERROR_SIGNAL.emit(e.args[0])
            return
        
    def Read(self):
        """Reads as much data from the server as possible. Returns the data
        as a binary stream of doubles (8 bytes floating point numbers, little-endian),
        where the order of the doubles is that all values of one sample time
        are grouped (i.e., [ ch1s1, ch2s1, ch3s1, ch1s2, ch2s2, ch3s3, ...]).
        This allows you to concatenate many returns of Flush into one large
        file, without losing the structure."""
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_DATA, 0, 0) )
        
        self._readUntilEmpty()
        self._sendall(header)
        self._assertHeader( (self.SYNC_ID, self.DATALOGGER_DATA) )
        
        if not self._isConnected:
            # If for some reason the connection was broken, don't try to parse the data
            return b''
            
        # Note that the communication to the server is in a totally different
        # format: big-endian, single precision (4 bytes), with channel-major 
        # order instead of sample-time-major order.
        nColumns, nSamples = self._unpackInts( self._recv(2*4) )
        self._debug("Reading data. nColumns: %d, nSamples: %d"%(nColumns, nSamples))
        if nColumns != len(self._channels):
            raise RuntimeError(self._namePrefix+"Number of channels on server (%d) differs from number of local channels (%d)."%(nColumns, len(self._channels)))
        N = nColumns*nSamples
        data = self._recv(4*N)
        data_np = np.ndarray(shape=(nColumns,nSamples),dtype='>f', buffer=data) # Does not copy the data. Each column belongs to one sample-time.
        bin=struct.pack("<%dd"%N,*(data_np.T.reshape(N,1))) # Transpose and reshape 
        
        if self._TimeSinceStartupIndex is not None:
            # We can find the most recent "#Lopes.TimeSinceStartup" value at byes values
            # (nColumns*(nSamples-1)+self._TimeSinceStartupIndex) * 8)
            i = (nColumns*(nSamples-1)+self._TimeSinceStartupIndex) * 8
            latestTimeSinceStartup = struct.unpack("<d",bin[i:i+8])
            print(latestTimeSinceStartup)
            
        return bin
        
    # BUFFER FUNCTIONS (This should've been a separate buffer class, but I found that out later)
    def StartReadingDataIntoBuffer(self):
        """Starts a separate thread which reads data into the buffer."""
        class ReadThread(QtCore.QThread):
            SAMPLE_TIME = 0.1
            def __init__(self,parent):
                QtCore.QThread.__init__(self)
                self._parent = parent
                self._lastTime = time.time()
                self._shouldStop = False
                
            def run(self):
                t = time.time()
                while not self._shouldStop:
                    self._lastTime = time.time()
                    self._parent.ReadFromServerToBuffer()
                    # Sleep until we're allowed to do the next query
                    t = time.time()
                    if (t-self._lastTime) < self.SAMPLE_TIME:
                        time.sleep(self.SAMPLE_TIME+self._lastTime - t)
                print(self._parent._namePrefix+"Exiting reader thread.")
                        
            def StopAndWait(self):
                self._shouldStop = True
                self.wait()
                
        self._readThread = ReadThread(self)
        self._readThread.start()
        
    def StopReadingDataIntoBuffer(self):
        """Sends a stops signal to the thread and waits for the thread to be stopped."""
        if self._readThread is not None:
            self._readThread.StopAndWait()
            self._readThread = None
        
        
    def ReadFromServerToBuffer(self):
        """Reads as much data from the server as possible. It reshuffles the data so that
        it is a binary stream of doubles (8 bytes floating point numbers, little-endian),
        where the order of the doubles is that all values of one sample time
        are grouped (i.e., [ ch1s1, ch2s1, ch3s1, ch1s2, ch2s2, ch3s3, ...]).
        This data is appended to an internal buffer. Returns the total number of samples in the
        internal buffer (thus, not the number of samples read).
        
        This allows you to concatenate many returns of Flush into one large
        file, without losing the structure."""
        header = self._packInts( (self.SYNC_ID, self.DATALOGGER_DATA, 0, 0) )
        
        self._readUntilEmpty()
        self._sendall(header)
        try:
            self._assertHeader( (self.SYNC_ID, self.DATALOGGER_DATA) )
        except RuntimeError as E:
            pass
            # If the connection was broken, _assertHeader gives a runtime error. We need to catch that, and
            # we'll automatically return in the next few lines
            
        if not self._isConnected:
            return 0
            
        # Note that the communication to the server is in a totally different
        # format: big-endian, single precision (4 bytes), with channel-major 
        # order instead of sample-time-major order.
        nColumns, nSamples = self._unpackInts( self._recv(2*4) )
        self._debug("Reading data. nColumns: %d, nSamples: %d"%(nColumns, nSamples))
        if nSamples==0:
            # No new data
            return 0 
        else:
            # Normal case
            if nColumns != len(self._channels):
                raise RuntimeError(self._namePrefix+"Number of channels on server (%d) differs from number of local channels (%d)."%(nColumns, len(self._channels)))
            N = nColumns*nSamples
            data = self._recv(4*N)
            data_np = np.ndarray(shape=(nColumns,nSamples),dtype='>f', buffer=data) # Does not copy the data. Each column belongs to one sample-time.
            bin=struct.pack("<%dd"%N,*(data_np.T.reshape(N,1))) # Transpose and reshape 
            
            self._dataBuffer=self._dataBuffer + bin
            
            return len(self._dataBuffer)//(nColumns*8)
    
    def GetNumberOfSamplesInBuffer(self):
        """Returns the current number of samples in the buffer."""
        return len(self._dataBuffer)//(len(self._channels)*8)
        
    def GetFirstTimestampInBuffer(self):
        """Returns the timestamp of the first sample in the buffer, or None if the buffer is empty."""
        if len(self._dataBuffer)==0:
            print(self._namePrefix+"Warning: Empty buffer while trying to sync")
            return None
        else:
            timeIndex = self._TimeSinceStartupIndex*8
            [timestamp,timestampWrap] = struct.unpack("<dd",self._dataBuffer[timeIndex:timeIndex+16]) # Read 2 doubles (both representing a single)
            
            # Calculate the correct time. This is, normally:
            # timestamp-rounded down-to-closest multiple of 10 + timestampwrap.
            # However, due to rounding error, it may be that this fails if timetamp is very close to a multiple of 10.
            # So, in that case, we round down to multiple-of-10 minus 5 (i.e., to 105, or 115, or 125, ...) and add
            # the timestampWrap plus 5 (modulo 10)
            if timestampWrap>2.5 and timestampWrap < 7.5:
                accurateTimestamp = timestamp-(timestamp%10) + timestampWrap
            else:
                accurateTimestamp = timestamp-(timestamp-5)%10 + (timestampWrap+5)%10
            return accurateTimestamp
            
    def RemoveSamplesFromBufferUntilTimestamp(self,timeStamp):
        """Removes all samples from the buffer that have a timestamp that is strictly
        less than timeStamp. Uses a dumb approach to do this."""
        sampleLength = len(self._channels)*8 # Number of bytes for each sample
        
        while len(self._dataBuffer)>0 and self.GetFirstTimestampInBuffer()<timeStamp:
            self._dataBuffer = self._dataBuffer[sampleLength:]
        
    def ReadOneFromBuffer(self):
        """Returns the first sample from the buffer and removes it out of the buffer. This function also stores the real time
        (as returned by GetFirstTimestampInBuffer) into the first double (replacing the original #SampleTime)."""
        sampleLength = len(self._channels)*8 # Number of bytes for each sample
        accurateSampleTime = self.GetFirstTimestampInBuffer()
        toReturn = struct.pack("<d",accurateSampleTime) + self._dataBuffer[8:sampleLength]
        self._dataBuffer = self._dataBuffer[sampleLength:] # Remove sample from buffer 
        return toReturn

    def ReadNFromBuffer(self,N):
        """Returns the first N samples from the buffer and removes them out of the buffer"""
        sampleLength = len(self._channels)*8 # Number of bytes for each sample
        toReturn = self._dataBuffer[0:(N*sampleLength)]
        self._dataBuffer = self._dataBuffer[(N*sampleLength):]
        return toReturn
        
    def GenerateDummyData(self,timestamp):
        """Generates a binary string of 'dummy' data. The data is compatible with
        ReadNFromBuffer(1), but instead of real data, it yields NaN's. The timestamp is
        encoded in the second element."""
        sampleLength = len(self._channels)
        values = [nan, timestamp] + (sampleLength-2) * [nan]
        return struct.pack("<%id"%sampleLength, *values) 

def TestLoggerConnection():
    """A small tester for the LoggerConnection class"""
    import time
    import visvis as vv
    configuration = defaultConfiguration.copy()
    L = LoggerConnection(configuration, print, name="TestConnection")
    L.ConnectToServer()
    L.Configure()
    L.RemoveAllChannels()
    L.AddChannel('sampleTime')
    L.AddChannel('a')
    L.AddChannel('b')
    L.Start()
    y=b''
    for i in range(20):
        time.sleep(0.04+0.03*i)
        y=y+L.Read()
        y=y+L.Read()
    L.Stop()
    # Let's show what we've got...
    yy = struct.unpack(">%dd"%(len(y)//8), y)
    t = yy[0::3]
    yy1= yy[1::3]
    yy2= yy[2::3]
    vv.clf()
    vv.plot(t,yy1,lw=2)
    vv.plot(t,yy2,lw=2,lc='r')

class ToggleLabel(QtGui.QLabel):
    """Label which has a toggled signal. Accepts two strings, which are used for state 0 and 1 respectively"""
    toggled = QtCore.Signal(int)
    
    def __init__(self,s0,s1,*args,**kwargs):
        if not isinstance(s0, str):
            raise TypeError("First argument should be a string")
        if not isinstance(s1, str):
            raise TypeError("Second argument should be a string")
        
        QtGui.QLabel.__init__(self,s0,*args,**kwargs)
        self._state = 0
        self._s0 = s0
        self._s1 = s1
        
    def state(self,s=None):
        """Set or get the current state. Note that the state is evaluated as a bool
        (so anything that evaluates to True gives state 1)."""
        if s is None:
            return self._state
        else:
            s = bool(s)
            previousState = bool(self._state)
            if s:
                self._state = 1
                self.setText(self._s1)
            else:
                self._state = 0
                self.setText(self._s0)
                
            if previousState != s:
                self.toggled.emit(self._state)
    def mousePressEvent(self, ev):
        self.state(not self._state)
        
class BlinkingLabel(QtGui.QLabel):
    """Label that implements blinking (i.e. text appears/disappears)"""
    def __init__(self, *args,**kwargs):
        QtGui.QLabel.__init__(self,*args,**kwargs)
        
        self._theText = QtGui.QLabel.text(self)
        self._blink = False # Whether we should blink
        self._blinkState = False # while blinking, is the text visible or not?
        self._blinkTimer = QtCore.QTimer()
        self._blinkTimer.timeout.connect(self._onBlinkTimer)
        self._blinkTimer.start(500)
    
    def blink(self):
        return self._blink
        
    def setBlink(self,v):
        """Use this function to enable/disable blinking."""
        self._blink = v
        if not v:
            QtGui.QLabel.setText(self,self._theText)
            
    def setText(self,s):
        self._blinkState = True
        self._theText = s
        QtGui.QLabel.setText(self, s)
        
    def text(self):
        return self._theText
        
    def _onBlinkTimer(self):
        if self._blink:
            self._blinkState = not self._blinkState
            if self._blinkState:
                QtGui.QLabel.setText(self,self._theText)
            else:
                QtGui.QLabel.setText(self,'')
          
class InputDialogTextEdit(QtGui.QInputDialog):
    """This class implements a new InputDialog (similar to the QInputDialog), which has a multiline editing window.
    Usage:
        defaultText = "This is a multi-line\ntext. Please edit me."
        result = InputDialogTextEdit.getText(None, "Window title","Label above the edit box", defaultText)
        if result[1]:
            print('Ok was pressed. The text was:\n%s'%result[0])
        else:
            print("Cancel was pressed")
    """
    def __init__(self, parent, title = 'TextEdit', labelText = 'Enter a value:',  text ='', *args, **kwargs):
        QtGui.QInputDialog.__init__(self, parent)#, *args, **kwargs)
        self.setModal(True)
        self.setWindowTitle(title)
        self.setLabelText(labelText)
        
        # Let's already create the QTextEdit. We cannot yet insert it into the 
        # widget because the widget is only created when we show() it.
        self._textEdit = QtGui.QTextEdit()
        self._textEdit.setText(text)
    def setSize(self,width,height):
        r = self.geometry()
        self.setGeometry(QtCore.QRect(300, 200, width, height))
        
    def show(self):
        QtGui.QInputDialog.show(self)
        self.setSize(700,700)
        theLineEdit = self.findChild(QtGui.QLineEdit)
        self.layout().removeWidget(theLineEdit)
        self.layout().insertWidget(1,self._textEdit)
        
        
    def textValue(self):
        return self._textEdit.toPlainText()
    def setTextValue(self,t):
        self._textEdit.setText(t)
        
    @staticmethod
    def getText(parent, title, label, text=''):
        """Static method to use this function"""
        form = InputDialogTextEdit(parent,title,label,text)
        form.show()
        result = form.exec()
        if result:
            return (form.textValue(),True)
        else:
            return ('',False)
    
class ChannelsListWidget(QtGui.QListWidget):
    """QListWidget with delete-key overridden, drag-drop functionality and a few extra convenience functions"""
    def __init__(self,*args,**kwargs):
        QtGui.QListWidget.__init__(self,*args,**kwargs)
        self.setAcceptDrops(True)

        self.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection ) # Allow selection of multiple
        self._undoStack = [] # A list, each item represents an undo state (i.e., all channels that were present at a certain moment.)
        self._redoStack = []
        
        self._undoButtonEnableFunctions = []
        self._redoButtonEnableFunctions = []
        self._duplicatesButtonEnableFunctions = []
        
    def SetUndoAndRedoEnableFunctions(self,undoButtonEnableFunctions, redoButtonEnableFunctions, dupButtonEnableFunctions, editAsTextButtonEnableFunctions):
        """This class has a undo/redo functionality. This means that you can
        attach buttons for undoing redoing to it (they should call the Undo() and Redo()
        function). When no more undo/redo steps are available, the buttons should be disabled.
        This function provides hooks for that. 
        Each argument should be a list of functions which accept one boolean argument; all these
        functions are called automatically by this class (they may be called more often than strictly necessary).
        The argument is True when there are undo/redo steps available and False otherwise.
        A good candidate for a function is myButton.setEnable.
        When this function is called, the undo/redo-enable functions are immediately called to update the states of the buttons."""
        self._undoButtonEnableFunctions = undoButtonEnableFunctions
        self._redoButtonEnableFunctions = redoButtonEnableFunctions
        self._duplicatesButtonEnableFunctions = dupButtonEnableFunctions
        self._editAsTextButtonEnableFunctions = editAsTextButtonEnableFunctions
        self._callEnableFunctions()
        
    def _callEnableFunctions(self):
        """Calls all Enable functions; see SetUndoAndRedoEnableFunctions. This is called automatically when the
        undo/redo-stacks change."""
        undoAvailable = len(self._undoStack)!=0 and self.isEnabled()
        redoAvailable = len(self._redoStack)!=0 and self.isEnabled()
        isDuplicates = self.GetNDuplicates()!=0 and self.isEnabled()
        
        for f in self._undoButtonEnableFunctions:
            f(undoAvailable)
        for f in self._redoButtonEnableFunctions:
            f(redoAvailable)
        for f in self._duplicatesButtonEnableFunctions:
            f(isDuplicates)
        for f in self._editAsTextButtonEnableFunctions:
            f(self.isEnabled())
            
    def _addToUndoStack(self, state = None, clearRedoStack = True):
        """if state is None, it adds the current state to the undo stack. Otherwise it adds state to the undo stack. Normally,
        adding something to this stack means that a new operation was done on the list. In this case, the redoStack should
        be emptied. The only exception is when a redo operation was done (then the redo stack should not be emptied)"""
        if clearRedoStack:
            self._redoStack.clear()
            
        if state is None:
            undoItem = self.AllChannelNames(True)
        else:
            undoItem = state
        self._undoStack.append(undoItem)
        self._callEnableFunctions()
        
    def _addToRedoStack(self, state = None):
        """See _addToUndoStack"""
        if state is None:
            redoItem = self.AllChannelNames(True)
        else:
            redoItem = state
        self._redoStack.append(redoItem)
        self._callEnableFunctions()
        
    def Undo(self):
        """Applies an 'undo' operation."""
        if not self.isEnabled():
            return
        if len(self._undoStack)!=0:
            self._addToRedoStack() # Add current state to the redo stack
            QtGui.QListWidget.clear(self)
            items = self._undoStack.pop()
            self.addItems(items)
        else:
            print("No more undo steps.")
        self._callEnableFunctions()
        
    def Redo(self):
        """Applies an 'undo' operation."""
        if not self.isEnabled():
            return
        if len(self._redoStack)!=0:
            self._addToUndoStack(clearRedoStack = False) # Add current state to the undo stack
            QtGui.QListWidget.clear(self)
            items = self._redoStack.pop()
            self.addItems(items)
        else:
            print("No more redo steps.")
        self._callEnableFunctions()
        
    def EditAsText(self):
        """Pops up an InputDialog which allows you to edit the channels as text."""
        channelsText = '\n'.join(self.AllChannelNames(True))
        resultText, isOk = InputDialogTextEdit.getText(self.parent(), "Channels list","Note: Not much error checking here!",channelsText)
        if isOk:
            items = [s.strip() for s in resultText.split('\n')]
            items = [s for s in items if len(s)!=0]
            self.ClearAndAddItems(items)
        
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key.Key_Delete:
            self.RemoveSelected()
        else:
            # Handle to superclass
            QtGui.QListWidget.keyPressEvent(self,event)
    
    def _getChannelNamesFromUrls(self, urls):
        """Given a list of QUrl's, it returns a list (possibly empty) of strings
        containing all channel names included in the urls."""
        
        addresses=[]; units = []
        for url in urls:
            if url.toString().endswith('.signals'):
                # Then we have a local .signals file
                filename = str(url.toLocalFile())
                file = open(filename,"rb")
                firstPartOfFile = ''
                while not firstPartOfFile.endswith(40*"="):
                    firstPartOfFile = firstPartOfFile+ file.read(40).decode('utf-8')
                file.close()
                firstPartOfFile = firstPartOfFile.rstrip("=")
                # now firstPartOfFile contains the (human-readable) header.
                ssdfData = ssdf.loads(firstPartOfFile)
                # The first two elements of the Signalsfile are the SampleTime and TimeSinceStartup, we don't need that.
                addresses.extend(["Lopes.%s"%n for n in ssdfData.map[2:]])
                units.extend(["[%s]"%u for u in ssdfData.units[2:]])
            elif url.toString().endswith('.txt'):
                # Then we have a local .signals file
                filename = str(url.toLocalFile())
                with open(filename) as file:
                    contents = file.readlines()
                    data = [x[1:].strip() for x in contents if x.startswith('#')]
                    addresses.extend( data)
                    units.extend([""] * len(data))
            else:
                # Then we probably have a webinterface url
                s = url.toString().replace("%28","(").replace("%29",")")
                parts = s.split("&")
                addresses.extend([p.split('=')[1].strip() for p in parts if p.startswith('address')])
                units.extend([p.split('=')[1].strip() for p in parts if p.startswith('unit')] )
        strings = [a+' '+u for a,u in zip(addresses,units)]
        return strings
        
    def dragEnterEvent(self, event):
        """This function tries to determine whether the data is useful. For the moment,
        we allow drag-drops of url's from the WebInterface. Not so much error-checking here!"""
        if event.mimeData().hasUrls():
            channelNames = self._getChannelNamesFromUrls(event.mimeData().urls())
            if len(channelNames)!=0:
                event.accept()
                return
        # If we're here, we don't have an acceptable item to be dropped
        event.ignore()

    def dragMoveEvent(self, event):
        # We 'll only get this event when we accepted the dragEnterEvent,
        # so no need to do extra checks here.
        event.accept()

    def dropEvent(self, event):
        """This function handles the drop of the url onto the listwidget. It parses
        the signals names and adds the signals to the list."""
        # If the url is a plot url from the webinterface, it has address# and unit#
        # elements. Let's use re's to parse them. I don't know how to get multple URL's
        # simultaneously, but at least this function supports it.
        channelNames = self._getChannelNamesFromUrls(event.mimeData().urls())
        self.AddItems(channelNames)
        event.accept()
            
    def Clear(self):
        self._addToUndoStack()
        QtGui.QListWidget.clear(self)
        self._callEnableFunctions()
        
    def RemoveSelected(self):
        self._addToUndoStack()
        for item in self.selectedItems():
            self.takeItem(self.row(item))
        self._callEnableFunctions()
    
    def ClearAndAddItems(self,items):
        self._addToUndoStack()
        QtGui.QListWidget.clear(self)
        self.addItems(items)
        self._callEnableFunctions()
        
    def AddItems(self,items):
        self._addToUndoStack()
        self.addItems(items)
        self._callEnableFunctions()
        
    def AddItem(self,item):
        self._addToUndoStack()
        self.addItem(item)
        self._callEnableFunctions()
        
    def setEnabled(self,v):
        QtGui.QListWidget.setEnabled(self,v)
        self._callEnableFunctions()
        
    def AllChannelNames(self, includeUnits):
        """Returns a list of strings containing all channel names (in order). If includeUnits is True,
        the string also contains the units (e.g., "Force [n])"""
        namesPlusUnits =  [self.item(i).text() for i in range(self.count())]
        if includeUnits:
            return  namesPlusUnits
        else:
            return [item.split('[')[0].strip() for item in namesPlusUnits]
            
    def AllUnits(self):
        """Returns a list of strings containing all units (in order)."""
        unitregs = [ re.match('.*\[(.*)\]',self.item(i).text()) for i in range(self.count())]
        units = [ r.group(1) if r is not None else "" for r in unitregs]
        return units
            
    def _list_duplicates(self,seq):
            """Returns a list of duplicate values, and also a list of indices of the duplicates."""
            
            seen = set()
            seen_add = seen.add
            # adds all elements it doesn't know yet to seen and all other to seen_twice
            seen_twice = set( x for x in enumerate(seq) if x[1] in seen or seen_add(x[1]) )
            if len(seen_twice)!=0:
                ind,val = zip(*seen_twice)
                return list( val ), list(ind)
            else:
                return [],[]
            
    def SelectDuplicates(self):
        """Selects all duplicates from the list (so that, if you press delete, there are no
        duplicates anymore). Returns the number of duplicates."""
        n = self.AllChannelNames(False)
        ndups, indices = self._list_duplicates(n)
        self.clearSelection()
        for i in indices:
            self.item(i).setSelected(True)
        self.setFocus()
        return len(indices)
        
    def GetNDuplicates(self):
        """Returns the number of duplicates."""
        n = self.AllChannelNames(False)
        ndups, indices = self._list_duplicates(n)
        return len(indices)
        
class LLCDataLogger(QtGui.QWidget):
    """The GUI for the whole thing."""        
    
    UPDATE_TIME = 100 # ms. How often do we request data from the server.
    
    def __init__(self,configuration):
        QtGui.QWidget.__init__(self)
        self._configuration = configuration.copy()
        # Parameters
        self._state = 0 # Can be either 
            # 0: 'idle', 
            # 1: 'armed'
            # 2: 'recording'
            # 3: 'idle-not-ready-for-arming'
            # 4: 'Connecting to server'
            # 5: 'Error' (need to restart)
            # 6: 'Arming'

        # DefaultSets of channels
        jointNames =  ["pelvis tX","pelvis tZ","left hip rX","left hip rZ","left knee rZ","left ankle rZ","right hip rX","right hip rZ","right knee rZ","right ankle rZ"]
        jointUnits = ['m','m','rad','rad','rad','rad','rad','rad','rad','rad']
        segmentNames = ["pelvis tX","pelvis tZ","left thigh rX","left thigh rZ","left shank rZ","left foot rZ","right thigh rX","right thigh rZ","right shank rZ","right foot rZ","pelvis tY","pelvis ry","pelvis rx"]
        segmentUnits = ['m','m','rad','rad','rad','rad','rad','rad','rad','rad','m','rad','rad']
        segmentUnitsAcc = ['m/s2','m/s2']+8*['rad/s2']+['m/s2','rad/s2','rad/s2']
        segmentForceUnits = ['N','N']+8*['Nm']
        self._defaultSets = ChannelSets
        self._channelsChanged = False
        self._plotWindows = [] # A list of plot/graph windows. (For the moment, only one window is allowed)
        self._plotLimiters = False
        self._valuesAtT0 = None
        self._valuesAtT0Time = None
        self._valuesAtT0TimeAsSting = " (not cached yet)"
        self._T = QtCore.QTimer()
        self._T.timeout.connect(self._onTimerTick)
        
        self._watchDogTimer = QtCore.QTimer()
        self._watchDogTimer.timeout.connect(self._onWatchDogTimerTick)
        self._watchDogTimer.start(2000)
        self._watchDogNReceived = 0 # This integer is increased when data is received and reset at watchdog
        
        self._lastTimeErrorMessage = [0,0,0] # The last time an error message about future/past samples was printed (we shouldn't do that too often); one element for each message type
        # Initialize GUI
        def SetField(fieldName,value): self._configuration[fieldName] = value
        self._ipLabel = ToggleLabel( \
            'IP settings (<span style="color:blue"><u>show</u></span>)', \
            'IP address / Logger port / Webinterface port (<span style="color:blue"><u>hide</u></span>)')
        self._ipAddrEdit = QtGui.QLineEdit(self._configuration['LLC_address'])
        self._ipLoggerPortEdit = QtGui.QLineEdit(str(self._configuration['Logger_port']))
        self._ipWebInterfacePortEdit = QtGui.QLineEdit(str(self._configuration['WebInterface_port']))
        # Make sure that configuration variable is updated when fields are changed
        self._ipAddrEdit.textChanged.connect(lambda v: SetField('LLC_address',v))
        self._ipLoggerPortEdit.textChanged.connect(lambda v: SetField('Logger_port',v))
        self._ipWebInterfacePortEdit.textChanged.connect(lambda v: SetField('WebInterface_port',v))
        # Allow IP settings to be shown/hidden
        def setIpVisible(v): 
            for e in [self._ipAddrEdit,self._ipLoggerPortEdit,self._ipWebInterfacePortEdit]: 
                e.setVisible(bool(v))
        self._ipLabel.toggled.connect(setIpVisible)
        setIpVisible(False)
            
        self._ipEditHBox = QtGui.QHBoxLayout()
        self._ipEditHBox.addWidget(self._ipAddrEdit,2)
        self._ipEditHBox.addWidget(self._ipLoggerPortEdit,1)
        self._ipEditHBox.addWidget(self._ipWebInterfacePortEdit,1)
        
        self._filePrefixLabel = QtGui.QLabel("Filename-prefix\n  (date+time+extension are added or put at the %d tag)")
        self._filePrefixEdit = QtGui.QLineEdit(self._configuration['file_prefix'])
        self._fileNameExample = QtGui.QLabel()
        self._plotCheckBox = QtGui.QCheckBox('Plot limiters')
        self._showPlotButton = QtGui.QPushButton('Show plot window')
        if not pyqtgraphAvailable:
            self._showPlotButton.setText("Show plot window (pyqtgraph not installed)")
            self._showPlotButton.setMaximumWidth(400)
        self._showPlotButton.setEnabled(False)
        self._showPlotButton.setMaximumWidth(200)

        # Modify button if pyqtgraph is not available
        if not pyqtgraphAvailable:
            self._showPlotButton.setText("Show plot window (pyqtgraph not installed)")
            self._showPlotButton.setMaximumWidth(350)
        
        self._showPlotButton.clicked.connect(self._onShowPlotButtonPressed)
        self._valuesAtT0Group = QtGui.QGroupBox('Values at t=0')
        self._valuesAtT0Vbox = QtGui.QVBoxLayout()
        self._valuesInclude = QtGui.QCheckBox('Include in signals file')
        self._valuesInclude.setChecked(True)
        self._valuesInclude.toggled.connect(self._stateChanged) # i.e., update GUI
        self._valuesModeLabel=QtGui.QLabel('Mode:')
        self._valuesModeLabel.setStyleSheet('margin-left:15px')
        self._valuesMode = QtGui.QComboBox()
        self._valuesMode.addItems(['Fast','Slow','Cache'])
        self._valuesMode.currentIndexChanged.connect(self._stateChanged) # i.e., update GUI
        self._valuesCacheButton = QtGui.QPushButton('Cache now')
        self._valuesCacheButton.setEnabled(False)
        self._valuesCacheButton.clicked.connect(self._cacheValuesAtT0)
        self._valuesHelpButton = QtGui.QPushButton('Help')
        self._valuesHelpButton.clicked.connect(self._showValuesAtT0Help)
        self._valuesModeHbox = QtGui.QHBoxLayout()
        self._valuesAtT0Group.setLayout(self._valuesAtT0Vbox)
        self._valuesModeHbox.addWidget(self._valuesModeLabel,0)
        self._valuesModeHbox.addWidget(self._valuesMode,1)
        self._valuesModeHbox.addWidget(self._valuesCacheButton,0)
        self._valuesModeHbox.addWidget(self._valuesHelpButton,0)
        self._valuesAtT0Vbox.addWidget(self._valuesInclude)
        self._valuesAtT0Vbox.addLayout(self._valuesModeHbox)
        self._enableAudioRecordingCheckBox = QtGui.QCheckBox("Record Audio")
        self._enableAudioRecordingCheckBox.setChecked(True)
        
        self._stateLabel = BlinkingLabel()
        self._stateLabel.setMinimumHeight(100); self._stateLabel.setMinimumWidth(100);
        self._stateLabel.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignCenter)
        self._fileLabel = QtGui.QLabel() # Filled later
        self._armButton = QtGui.QPushButton('Arm for\nrecord')
        self._recordButton = QtGui.QPushButton('Start\nrecording')
        self._stopButton = QtGui.QPushButton('Stop\nrecording')
        self._buttonBox = QtGui.QHBoxLayout()
        for b in [self._armButton, self._recordButton, self._stopButton]:
            self._buttonBox.addWidget(b)
            b.setMinimumHeight(100)
            
        self._defaultSetsLabel = QtGui.QLabel("Sets of channels to add...")
        self._defaultSetsCombobox = QtGui.QComboBox()
        for s in self._defaultSets: self._defaultSetsCombobox.addItem(s[0])
        self._channelsLabel = QtGui.QLabel("Channels which will be recorded:")
        self._channelsList = ChannelsListWidget()
        self._undoButton = QtGui.QPushButton("Undo")
        self._redoButton = QtGui.QPushButton("Redo")
        self._editAsTextButton = QtGui.QPushButton("Edit as text")
        self._dupButton = QtGui.QPushButton("Dup's")
        self._dupButton.setToolTip('Select duplicates in channel list (disabled: no duplicates)')
        self._channelsButtonBox= QtGui.QHBoxLayout()
        for b in [self._editAsTextButton, self._undoButton, self._redoButton, self._dupButton]:
            self._channelsButtonBox.addWidget(b)
        self._channelsList.SetUndoAndRedoEnableFunctions([self._undoButton.setEnabled], [self._redoButton.setEnabled], [self._dupButton.setEnabled], [self._editAsTextButton.setEnabled])
        
        
        self._left = QtGui.QVBoxLayout()
        self._left.addWidget(self._ipLabel)
        self._left.addLayout(self._ipEditHBox)
        self._left.addWidget(self._filePrefixLabel)
        self._left.addWidget(self._filePrefixEdit)
        self._left.addWidget(self._fileNameExample)
        self._left.addWidget(self._plotCheckBox)
        self._left.addWidget(self._showPlotButton)
        self._left.addWidget(self._valuesAtT0Group)
        self._left.addWidget(self._enableAudioRecordingCheckBox)
        self._left.addStretch()
        self._left.addWidget(self._stateLabel)
        self._left.addWidget(self._fileLabel)
        self._left.addLayout(self._buttonBox)
        self._right = QtGui.QVBoxLayout()
        self._right.addWidget(self._defaultSetsLabel)
        self._right.addWidget(self._defaultSetsCombobox)
        self._right.addWidget(self._channelsLabel)
        self._right.addWidget(self._channelsList)
        self._right.addLayout(self._channelsButtonBox)
        self._hb = QtGui.QHBoxLayout()
        self._hb.addLayout(self._left)
        self._hb.addLayout(self._right)
        
        # Connect signals
        self._filePrefixEdit.textChanged.connect(self._onPrefixChanged)
        self._plotCheckBox.clicked.connect(self._onPlotCheckBoxChanged)
        self._armButton.clicked.connect(self._onArmButtonPressed)
        self._recordButton.clicked.connect(self._onRecordButtonPressed)
        self._stopButton.clicked.connect(self._onStopButtonPressed)
        self._defaultSetsCombobox.currentIndexChanged.connect(self._onDefaultSetChanged)
        self._editAsTextButton.clicked.connect(self._channelsList.EditAsText)
        self._undoButton.clicked.connect(self._channelsList.Undo)
        self._redoButton.clicked.connect(self._channelsList.Redo)
        self._dupButton.clicked.connect(self._channelsList.SelectDuplicates)
        
        self.SetFontSize(12)
        self.setLayout(self._hb)
        self._stateChanged() # Update gui 
        self._onPrefixChanged()
        self.setWindowTitle('LLC Data Logger')
        self.setIcon('Gray')
        self.show()
        
        
    def setIcon(self,color):
        """Sets the window icon to a specific color (Gray, Red, Green, Yellow). Note that
        the names are case-sensitive"""
        
        if not hasattr(self,'_icons'):
            # Then this is the first time that this function was called; do a one-time initialization
            
            # The following lines assure that the application gets its own icon on the
            # the Taskbar in Windows 7 (as opposed to being grouped with other Python
            # applications)
            try:
                import ctypes
                myappid = 'LLCDataLogger' # arbitrary string
                ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
            except:
                pass # Allow testing on other platforms
                
            # Get all icons
            availableColors = ['Red','Green','Yellow','Gray']
            self._icons = dict([(c, QtGui.QIcon('LLCDataLogger%s.ico'%c)) for c in availableColors])
            
        self.setWindowIcon(self._icons[color])
        
    def _catchExceptionDecorator(f):
        def wrapper(self,*args,**kwargs):
            try:
                f(self,*args,**kwargs)
            except Exception as E:
                self._setState(5) # Set to error state
                
                import traceback
                titlePart = "A Python Exception was raised"
                helpPart = traceback.format_exc()
                exceptionString = traceback.format_exc().splitlines()[-1]
                bodyPart = "A Python Exception was raised during the execution of a callback. Click 'More help' to see the exception.\n\n%s"%exceptionString
                
                self._showSevereError(titlePart, bodyPart, helpPart)
                
        return wrapper
        
    def _connectToServerAndConfigure(self):
        try:
            self._C.ConnectToServer()
            self._C.Configure()
        except OSError:
            # Failed to connect
            print("Failed to connect to server.")
            return # We will try again in a few seconds
        
        # If we're here, we successfully connected
        self._connectTimer.stop()
        self._setState(0)
        
    def _stateChanged(self):
        """Updates the gui when the state of the system (idle, armed, recording) changes."""
        st = self._state
        stateLabelStylesheet = 'font-size: 30px;border-width:1pt;background-color:%s'
        self._stateLabel.setStyleSheet(stateLabelStylesheet%['lightgray','yellow','red', 'lightgray','lightgray','lightgray','lightgray'][st])
        self._stateLabel.setText(['idle','armed for record','recording', 'idle','not connected\nto server','error,\nplease restart','arming'][st])
        self._stateLabel.setBlink([False, False, True, False, False, False, True][st])
        self._armButton.setText(['Arm for\nrecording','Unarm\n','Unarm\n','Arm for\nrecording','Arm for\nrecording','Arm for\nrecording','Unarm\n'][st])
        self._plotCheckBox.setEnabled(True)
        
            
        self._armButton.setEnabled       ([True,  True,  False, False, False, False, False][st])
        self._recordButton.setEnabled    ([False, True,  False, False, False, False, False][st])
        self._stopButton.setEnabled      ([False, False, True,  False, False, False, False][st])
        self._ipAddrEdit.setEnabled      ([True,  False, False, True,  True, True, False][st])
        self._ipLoggerPortEdit.setEnabled([True,  False, False, True, True, True, False][st])
        self._ipWebInterfacePortEdit.setEnabled    ([True, False, False, True, True, True, False][st])
        self._filePrefixEdit.setEnabled  ([True,  True, False, True, True, True, True][st])
        self._showPlotButton.setEnabled  ([False, pyqtgraphAvailable, pyqtgraphAvailable,  False, False, False, pyqtgraphAvailable][st])
        
        valuesAtT0GlobalEnable = ([True, True, False, True, True, True, True][st])
        
        self._valuesInclude.setEnabled(valuesAtT0GlobalEnable)
        self._valuesMode.setEnabled( self._valuesInclude.isChecked() and valuesAtT0GlobalEnable)
        self._valuesCacheButton.setEnabled( self._valuesInclude.isChecked() and self._valuesMode.currentText()=='Cache' and valuesAtT0GlobalEnable )
        self._valuesInclude.setText("Include in signals file"+ (self._valuesAtT0TimeAsSting if self._valuesMode.currentText()=='Cache' else ""))
        self._enableAudioRecordingCheckBox.setEnabled([True,  True, False, True, True, False,True][st])
        
        
        self._defaultSetsCombobox.setEnabled([True, False, False, True, True, True, False][st])
        self._channelsList.setEnabled([True, False, False, True, True, True, False][st])
            # 0: 'idle', 
            # 1: 'armed'
            # 2: 'recording'
            # 3: 'idle-not-ready-for-arming'
            # 4: 'Connecting to server'
            # 5: 'Error' (need to restart)
            # 6: 'Arming'
        
        self.setIcon(['Gray','Yellow','Red', 'Gray','Gray','Gray','Gray'][st])
        if st==1:
            self._armButton.setFocus()
        
    def SetFontSize(self,fs):
        """Sets the font size of all elements of the GUI. If fs is a string starting with + or -,
        it sees it as a relative size"""
        if isinstance(fs,str):
            if fs[0]=='-':
                self._fontSize -= int(fs[1:])
            elif fs[0]=='+':
                self._fontSize += int(fs[1:])
            else:
                raise RuntimeError("Invalid string for SetFontSize (expected '+<no>' or '-<no>' but received '%s'"%fs)
        else:
            self._fontSize = fs
            
        self.setStyleSheet('font-size: %dpt'%self._fontSize)
                
    def _channelsToGroupedChannels(self, channels, units, addHash, addTimingChannels, exportAsGrouped):
        """Given arguments
           channels = a list of strings (either with or without unit),
           units = a list of strings containing the unit of each channel OR [] OR None if not needed,
           addHash = False or True
           addTimingChannels = "no" or "once" or "all"
           exportAsGrouped = False or True
        this function returns either:
        - A list of lists, where each sublist contains the channels 
          "sampleTime", "#Lopes.TimeSinceStartup", "#Lopes.TimeSinceStartupWrapped" (depending on the value of addTimingChannels) and a few channels from the original channel list,
          if exportAsGrouped is True
        - The above with the following differences:
          + all lists concatenated so that it is one list again,
          if exportAsGrouped is False.
        - A tuple (C,U) where C and U are channels/units list as described above if units is not None or []
        Behavior for addTimingChannels:
        - "no": No "sampleTime" and "#LopesTimeSinceStartup", "#Lopes.TimeSinceStartupWrapped" channels are added anywhere
        - "once": Only as very first three channels they are added (i.e., only for the first logger)
        - "all": for each logger, i.e., for each list when exportAsGrouped is True, the channels are added.
        - anything else: ValueError()
        Additionally, if addHash is True, then "#" signs are prepended to each of the channels lists.
        """
        MAX_CHANNELS = 29
        
        # Check value of addTimingChannels
        if not addTimingChannels in ["no","once","all"]:
            raise ValueError("Invalid value for addTimingChannels: %s"%addTimingChannels)
            
        # Prepare channels list
        c2 = channels[:] # Make a copy so that I don't ruin channels
        if addHash:
            c2 = ["#%s"%s for s in c2]
        
        # Check what to do with variable units. In order to make the rest of the function easy,
        # we'll make units such that it can be processed, and if not needed, we'll throw away the
        # result
        if units is None or (type(units)==list and len(units)==0):
            # Create a list of empty strings
            u2 = len(channels)*['']
            returnUnits = False
        elif type(units)==list and len(units)==len(channels):
            # This means that units was correct
            u2 = units[:]
            returnUnits = True
        else:
            raise RuntimeError("variable units was not correct type or correct length.")
        
        
        # Group them
        channelsGrouped = [c2[n:n+MAX_CHANNELS] for n in range(0, len(c2), MAX_CHANNELS)]
        unitsGrouped    = [u2[n:n+MAX_CHANNELS] for n in range(0, len(u2), MAX_CHANNELS)]
        
        
        # Prepend sampleTime and "#Lopes.TimeSinceStartup" if needed
        if addTimingChannels=="once":
            channelsGrouped[0].insert(0, "sampleTime")
            channelsGrouped[0].insert(1, "#Lopes.TimeSinceStartup")
            channelsGrouped[0].insert(2, "#Lopes.TimeSinceStartupWrapped")
            unitsGrouped[0].insert(0, 's')
            unitsGrouped[0].insert(1, 's')
            unitsGrouped[0].insert(2, 's')
        elif addTimingChannels=="all":
            for c in channelsGrouped:
                c.insert(0, "sampleTime")
                c.insert(1, "#Lopes.TimeSinceStartup")
                c.insert(2, "#Lopes.TimeSinceStartupWrapped")
            for u in unitsGrouped:
                u.insert(0, 's')
                u.insert(1, 's')
                u.insert(2, 's')
        # else: addTimingChannels must be "no", so don't do anything

            
        if exportAsGrouped:
            if returnUnits:
                print(channelsGrouped)
                print(unitsGrouped)
                return (channelsGrouped,unitsGrouped)
            else:
                print(channelsGrouped)
                return channelsGrouped
        else:
            # Export as flattened list
            if returnUnits:
                cc = list(itertools.chain.from_iterable(channelsGrouped))
                uu = list(itertools.chain.from_iterable(unitsGrouped))
                print(cc)
                print(uu)
                return (cc,uu)
            else:
                cc = list(itertools.chain.from_iterable(channelsGrouped))
                print(cc)
                return cc
            
    def _onArmButtonPressed(self):
        if self._state == 0:
            self._arm()
        elif self._state == 1: # armed
            self._unArm()    
        else:
            raise RuntimeError("Cannot use this button in state %d"%self._state)

    def _cacheValuesAtT0(self, mode='Fast'):
        """This function will cache some values from the LLC as 'ValuesAtT0'. The
        main use is to store settings, such as patient dimensions. The way the
        values are obtained differes for various values. Values that can be
        obtained via the UDP Api are done as such (this is fast and safe). However,
        most can only be obtained by using the Webinterface. This can be done in
        two ways: 'Fast' (as fast as possible, with the risk of severe hickups')
        or 'Slow' (with delays between the requests, which may give multiple smaller
        hickups). All data is stored in a private variable self._ValuesAtT0. This variable
        is a dict and can directly be included in the ssdf header of the log file.
        """
        def AsList(s):
            """Converts string to list of values"""
            return [float(x) for x in s.replace('[','').replace(']','').split(',')]
            
        if not mode in ['Fast','Slow']:
            raise ValueError("Mode should be either 'Fast' or 'Slow' (but was %s)"%mode)
        
        if mode=='Slow':
            delay = 0.3
            Dlg = QtGui.QLabel("Reading values at t=0;\nplease wait.")
            Dlg.setStyleSheet('font-size:%dpt; margin:20px'%self._fontSize)
            Dlg.setAlignment(QtCore.Qt.AlignCenter)
            Dlg.setWindowTitle("LLC Data Logger")
            Dlg.show()
            QtGui.QApplication.processEvents()
        else:
            delay = 0.0
            
        theTime = datetime.datetime.now()
        
        # First do the UDP parts
        L = LopesUDPInterface( self._configuration['LLC_address'], isPrint = False)
        patientDim = L.Send('get patientdim')
        patientDimKeys = ['Mass','Length','PelvisWidth','PelvisHeight', \
            'ThighLengthLeft','ThighLengthRight','ShankLengthLeft','ShankLengthRight', \
            'FootLengthLeft','FootLengthRight','FootHeightLeft','FootHeightRight']
            
        state = L.Send('get state').replace('"','')
        L.Close()
        
        # Now the WebInterface parts
        url = 'http://'+self._configuration['LLC_address']+':'+str(self._configuration['WebInterface_port'])+'/goform/formStats'
        W = WebInterfaceInterface(url,False)
        massModel = W.GetCurrentMassModel()
        time.sleep(delay)
        cartesianInertia = W.GetCartesianInertia(True)
        time.sleep(delay)
        segmentInertia = W.GetSegmentInertia(True)
        time.sleep(delay)
        cartesianInertiaScaling = W.GetCartesianInertiaScaling(True)
        time.sleep(delay)
        segmentInertiaScaling = W.GetSegmentInertiaScaling(True)
        time.sleep(delay)
        accRef, accRefDict = W.GetAccelerationRef(True)
        # Gather in one dict
        v = dict()
        v['rawData'] = {'PatientDim':patientDim, 'GlobalAccRefGain':accRef[0], \
            'AccelerationRef':accRef[1:], 'CurrentMassModel':massModel, \
            'CartesianInertia': cartesianInertia[0],
            'SegmentInertia': segmentInertia[0],
            'CartesianInertiaScaling': cartesianInertiaScaling[0],
            'CartesianInertiaScaling': segmentInertiaScaling[0],
            'State': state
            }
        
        v['PatientDim'] = dict(zip(patientDimKeys,patientDim))
        v['AccelerationRef'] = accRefDict
        v['State'] = state
        v['CurrentMassModel'] = massModel
        v['CartesianInertia'] = cartesianInertia[1]
        v['SegmentInertia'] = segmentInertia[1]
        v['CartesianInertiaScaling'] = cartesianInertiaScaling[1]
        v['CartesianInertiaScaling'] = segmentInertiaScaling[1]
        
        self._valuesAtT0 = v
        self._valuesAtT0Time = theTime
        self._valuesAtT0TimeAsSting = theTime.strftime(" (cached at %H:%M:%S)")
        
        if mode=='Slow':
            Dlg.hide()
        
        self._stateChanged() # Update GUI (needed for updating the time of caching)
        # Don't return anything; the data is stored in a private member

    def _arm(self):
        # Arm
        
        # First check a few things to see whether we can really arm
        try:
            # Check whether the port represents an integer
            self._configuration['Logger_port'] = int(self._configuration['Logger_port'])
        except ValueError:
            ret = QtGui.QMessageBox.warning(self, "LLC Data Logger",
                            "Error: IP Port should be an integer (default: 4321).",
                            QtGui.QMessageBox.Ok)
            return
            
        if self._channelsList.count() ==0:
            ret = QtGui.QMessageBox.warning(self, "LLC Data Logger",
                            "Error: No signals to log.",
                            QtGui.QMessageBox.Ok)
            return
        if self._channelsList.GetNDuplicates()!=0:
            ret = QtGui.QMessageBox.warning(self, "LLC Data Logger",
                "Error: There are duplicate signals. Use the 'dups' button to select them and delete them.",
                QtGui.QMessageBox.Ok)
            return
            
        # Target online (by doing a ping)
        if not ping(self._configuration['LLC_address']):
            ret = QtGui.QMessageBox.warning(self, "LLC Data Logger",
                "Error: Could not reach target IP %s (tried with ping). Check IP settings."%self._configuration['LLC_address'],
                QtGui.QMessageBox.Ok)
            return
            
        
        # Check if there are any effect channels, and if so, if they exist
        allEffects = list(set([c.split('.')[2] for c in self._channelsList.AllChannelNames(False) if c.split('.')[1]=='HapticRenderer']))
        # The ZeroImpedanceJointSpring is always available but is not recognized by the get isEffect command.
        # Therefore, we can (and should) not check its availability and just assume that it exists (which it will)
        allEffects = [a for a in allEffects if a !='ZeroImpedanceJointSpring']
        
        if len(allEffects)!=0:
            L = LopesUDPInterface(self._configuration['LLC_address'], isPrint = False)
            for e in allEffects:
                try:
                    response = L.Send('get isEffect %s'%e)
                except:
                    # Timeout; probably the LLC UDP interface didn't exist, for example when using DataLoggerTester
                    # Just act as if the effect is there
                    response = '"true"'
                        
                if response != '"true"':
                    # Then the effect did not exist
                    ret = QtGui.QMessageBox.warning(self, "LLC Data Logger",
                        "Error: The effect %s does not exist.\nPlease create the effect before arming."%e,
                        QtGui.QMessageBox.Ok)
                    L.Close()
                    return
            L.Close()
            
        self._setState(6) # Arming state
        QtCore.QCoreApplication.processEvents();
        
        MAX_CHANNELS = 30    
        # Connect to server and add all channels
        # If we're logging more than 30 channels, we need multiple loggers.
        channelsGrouped = self._channelsToGroupedChannels(self._channelsList.AllChannelNames(False), None ,True, "all", True)
            
        self._nLoggers = len(channelsGrouped)
        self._channelsGrouped = channelsGrouped
        self._channelsGroupedFlattened = self._channelsToGroupedChannels(self._channelsList.AllChannelNames(False), None ,True, "all", False)
        
        # Generate list of map (i.e., channel names) and units
        (self._map,self._units) = self._channelsToGroupedChannels(self._channelsList.AllChannelNames(False), self._channelsList.AllUnits(), False, "all", False)
        # Remove leading "Lopes." (and #Lopes. ) from field names
        self._mapSimpleNames = [ re.match('(?:#?Lopes\.)?(.*)',m).group(1) for m in self._map ]
        
        # Create list of all channels that have something to do with limiters
        self._limiterChannelNumbers = [x[0] for x in enumerate(G._channelsGroupedFlattened) if 'imit' in x[1]]
        self._limiterChannelNames = [x[1] for x in enumerate(G._channelsGroupedFlattened) if 'imit' in x[1]]
        
        self._C=[]
        for i in range(self._nLoggers):
            C=LoggerConnection(self._configuration, print, name="Logger%i"%i)
            C.ConnectToServer()
            C.Configure()
            self._C.append(C)
        
        for i in range(self._nLoggers):
            C = self._C[i]
            C.RemoveAllChannels()
            for n in channelsGrouped[i]:
                C.AddChannel(n)
                QtCore.QCoreApplication.processEvents();
                time.sleep(0.02)
        
        
        # Temporary: create debug file for non-synced data
#         fileName = self._generateFilename(maskTime = False, theTime = datetime.datetime.now())
#         self._debugFiles = [ open(fileName.replace('.signals','_debug%i.signals'%i),'wb') for i in range(len(self._C))]
        
        
        self._isFirst = True
        # Create timer; start logging
        for C in self._C:
            C.Start()
            time.sleep(0.1);
            C.StartReadingDataIntoBuffer()
        self._T.start(self.UPDATE_TIME)
        self._setState(1) # Set to Armed mode
        
        # Prepare plot for limiters
        if self._plotCheckBox.isChecked():
            vv.clf()
            self._p = vv.plot([],[],aa=0,ms='.',mw=1)
            # Add floating label for showing individual values
            self._CreateLimiterWindow()
            self._plotLimiters = True
        
    def _unArm(self):
        # Stop logging
        for C in self._C:
            C.StopReadingDataIntoBuffer()
            C.Stop()
            C.DisconnectFromServer()
        self._T.stop()
        
        # Delete any plot windows (we cannot leave them open, because next time at arming the channels may have changed)
        self.RemoveAllPlotWindows()
        
#         for f in self._debugFiles:
#             f.close()
#         self._debugFiles = None

        # Unarm
        self._setState(0)
        
    def _CreateLimiterWindow(self):
        self._limitsTextWindow = l = QtGui.QTextEdit()
        l.setReadOnly(True)
        l.show()
        l.setWindowTitle('Limiters')
        l.setFont('courier')

    @_catchExceptionDecorator
    def _onRecordButtonPressed(self):
        # If Include mode is Cache, check whether there is cached data
        if self._valuesInclude.isChecked() and self._valuesMode.currentText() == 'Cache' and self._valuesAtT0Time is None:
            # Then we cannot start, so give an error message
            ret = QtGui.QMessageBox.warning(self, "LLC Data Logger",
                "Error: Values at t=0 were not cached yet. Press 'Cache' button before starting the recording (or choose a different mode).",
                QtGui.QMessageBox.Ok)
            return
            
        # Create file
        self._startTime = datetime.datetime.now()
        self._fileName = self._generateFilename(maskTime = False, theTime = self._startTime)
        self._file = open(self._fileName,'wb')
        
        # Create Audio recording class
        if self._enableAudioRecordingCheckBox.isChecked():
            wavFileName = self._fileName.replace(self._configuration['file_extension'],'.wav')
            self._WavRecorder = WavRecorder(wavFileName)
            self._WavRecorder.StartRecording()
            
        # Get Values at T0 (if needed)
        mode = self._valuesMode.currentText()
        if self._valuesInclude.isChecked():
            if mode in ['Slow','Fast']:
                self._cacheValuesAtT0(mode)
            # otherwise, mode is Cached, and we don't need to read the values right now, just use the cached values
            self._writeSignalsFileHeader({'valuesAtT0': self._valuesAtT0})
        else:
            # No ValuesAtT0
            self._writeSignalsFileHeader()
        
        # Update GUI
        self._fileLabelFileText = "Recording to file %s"%self._fileName # Used in the onTimetTick to show a nice string.
        
        self._setState(2)

    def _onStopButtonPressed(self, isError=False):
        
        if self._state==2: # Then we were recording
            self._setState(1) # Set to armed again. This makes the onTimer stop writing to the file
            
            if self._enableAudioRecordingCheckBox.isChecked():
                self._WavRecorder.StopRecording()
                self._WavRecorder.UpdateFileSizeAndCloseFile()
            # Close file and remove references to it
            if not isError:
                self._file.write("\r\n".encode()) # Closing string to indicate that the file was closed correctly
                
            else:
                # Don't write a footer
                pass
            self._file.close()
            self._file = None
            self._fileLabel.setText("Last file saved: %s %s"%(self._fileName,"stopped because of error" if isError else ""))
            self._fileName = None
                           
    def _setState(self,newState):
        self._state = newState
        self._stateChanged()
                
    def _generateFilename(self, maskTime = False, theTime=None):
        """Returns a filename with timestamp. If maskTime is true,
        it will return a string where the time part is replaced by xxx's.
        if theTime is given, then that datetime is taken instead of datetime.datetime.now()"""
        if theTime is None:
            theTime = datetime.datetime.now()
        format = "%Y%m%d_xxxxxx" if maskTime else "%Y%m%d_%H%M%S"
        timestamp= datetime.datetime.strftime(theTime,format)
        if '%d' in self._filePrefixEdit.text():
            return self._filePrefixEdit.text().replace('%d',timestamp)+self._configuration['file_extension']
        else:
            return self._filePrefixEdit.text()+timestamp+self._configuration['file_extension']
    
    def _onPlotCheckBoxChanged(self):
        if self._plotCheckBox.isChecked():
            # Show the plot
            if self._state!=0:
                # Prepare plot for limiters
                self._p = vv.plot([],[],aa=0,ms='.',mw=1)
                self._CreateLimiterWindow()
                self._plotLimiters = True
        else:
            # Hide it
            self._plotLimiters = False
            gcf = self._p.parent.parent.parent
            vv.close(gcf)  
            self._limitsTextWindow.hide()
            
    def AddPlotWindow(self):
        """Creates a plot window and adds it to the list of plot windows. For
        convenience, it returns a pointer to the created window"""
        
        # Only allow in certain states
        assert (self._state in [1,2,6])
        
        # Create it
        p = Graph(self._mapSimpleNames, self._units)
        self._plotWindows.append(p)
        return p
        
    def RemoveAllPlotWindows(self):
        """Removes all plot windows (i.e., the Qt objects) and removes them from
        the list of plot windows."""
        for p in self._plotWindows:
            p.deleteLater() # This closes the window and makes Qt remove the Qt (C++) object
            
        self._plotWindows = [] # Remove all references to all plot windows at once.
        
    def _onShowPlotButtonPressed(self):
        """Create or show the plot window. The button is only visible when the system is armed."""
        if len(self._plotWindows)==0:
            # Then there is no plot window yet, so create it.
            p=self.AddPlotWindow()
            p.show()
        else:
            # Then there was alreay a window (which may have been closed - but not deleted)
            self._plotWindows[0].show()
            
    def _onPrefixChanged(self):
        """Updates the GUI when prefix was changed."""
        fileName = self._generateFilename(maskTime = False)
        s = "Example: %s"%fileName
        if not os.path.isdir(os.path.dirname(fileName)):
            s = s + "\nNote: %s is not a directory"%os.path.dirname(fileName)
            self._fileNameExample.setStyleSheet('color:red')
            self._fileNameExample.setText(s)
            if self._state==0:
                self._setState(3)
        else:
            self._fileNameExample.setStyleSheet("")
            self._fileNameExample.setText(s)
            if self._state==3:
                self._setState(0)
        
    def _onDefaultSetChanged(self, i):
        """Adds a set of parameters to the parameter list"""
        channels = self._defaultSets[i][1]
        self._channelsList.AddItems(channels)
                                            
    def _showSevereError(self,titlePart, bodyPart, helpPart):
        """This function makes very clear to the user that something was wrong with
        the connection. It will make the appplication blink read, shows an on-top 
        error window and plays sounds until the user responds."""
        import re
        try:
            import winsound
            playSound = True
        except:
            print("Warning: Cannot play sound!")
            playSound = False
            
        def Flicker(SetToDefault=False):
            """Changes the background color of the application."""
            s = self.styleSheet().split(';')
            if any( ['background-color' in x for x in s]) or SetToDefault:
                s = [x for x in s if 'background-color' not in x] # Remove background color element
            else:
                # Add background color
                s.append('background-color:red')
            self.setStyleSheet(';'.join(s))
            
        def Sound():
            # Play a short sound
            if playSound:
                winsound.MessageBeep()
                
        FlickerTimer=QtCore.QTimer()
        FlickerTimer.timeout.connect(Flicker)
        FlickerTimer.start(200)
        SoundTimer = QtCore.QTimer()
        SoundTimer.timeout.connect(Sound)
        SoundTimer.start(1500)
        
        # Present message box
        fullTitle = "LLC Data Logger: "+titlePart
        fullBody = bodyPart + "\n\nPlease close and restart the application."
        
        Msg = QtGui.QMessageBox(QtGui.QMessageBox.Critical,fullTitle,fullBody)
        OkButton = Msg.addButton(QtGui.QMessageBox.Ok)
        HelpButton = Msg.addButton("More help",QtGui.QMessageBox.HelpRole)
        Msg.setStyleSheet('background-color:none') # Override color so that it does not blink red
        Msg.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        Msg.exec_()
        
        SoundTimer.stop()
        FlickerTimer.stop()
        Flicker(SetToDefault=True)
        
        if Msg.clickedButton() == HelpButton:
            # Then show another window with more help
            helpText = helpPart + "\n\n" + \
            "If you don't want to lose your list of signals, press the 'Edit as text' button and " + \
            "copy the text onto the clipboard. Then restart the application, choose 'Edit as text' again and paste."
            msg2 = QDialogWithTextEdit(None,fullTitle,helpText)
            msg2.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
            msg2.exec_()

    def _showValuesAtT0Help(self):
        """Shows a dialog box with some help text"""
        title = """LLC Data Logger: WebInterface setting"""
        helpText = """The logger can log some 'initial' values at the start of logging. Examples are the patient dimensions \
        and the mass model properties. Most of these can only be obtained via the WebInterface. A disadvantage of this is that \
        this may cause hickups in the movement of the robot. Hence, it may not be a good idea to do this while walking \
        (especially if you don't want to disturb the gait). Depending on what you want, you can choose between the following \
        settings for interacting via the WebInterface:
        <ul style="spacing:0px"><li><b>Fast:</b> Requests the data as fast as possible. Minimum delay but maximum chance of hickups. Perfect for usage when \
        the robot is not moving anyway..
        <li><b>Slow:</b> Requests the data with delays between subsequent requests. Takes longer and may give multiple small hickups.
        <li><b>Cached:</b> You can request the data manually by pressing the button 'Cache now' (may give hickups but you can do this when this doesn't matter).
            This data is then used for all subsequent logs.
        </ul>"""
        helpText = re.sub("\s+"," ",helpText) # Remove multiple spaces
        Msg = QtGui.QMessageBox(QtGui.QMessageBox.Information,title,helpText, QtGui.QMessageBox.Ok)
        Msg.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        Msg.exec_()

    def _onWatchDogTimerTick(self):
        """Checks whether we still get data. If not, we issue an error (audible and visible)
        and go back to the Unarmed state."""
        if self._state in [1,2]: # Armed or recording
            if self._watchDogNReceived <= 0:
                # Then we didn't receive any data; that is bad! Stop recording and go back to idle mode
                self._onCommunicationError("No data received for too long period (LLCDataLogger-Watchdog timeout)")
            else:    
                self._watchDogNReceived = 0
        else:
            self._watchDogNReceived = 1 # This makes sure that, even when the watchdogtimer came
                # immediately after going to state 1 or 2, we won't get an error right away.
                # (but only after it had a chance to get data)
            
    def _onCommunicationError(self,s):
        """Called by a signal from the Communication channel class or the watchdog if something went wrong during
        communication."""
        
        if self._state==5:
            # Then the error was already 'known'.
            return
        
        if self._state == 2: # Then we were recording. Try stopping the recording
            self._onStopButtonPressed(isError=True)
        self._unArm()
            
        self._setState(5)
        # We do not do any attempt to handle the error correctly; we just suggest to restart the application. Is much
        # simpler.
        titlePart = "Connection lost!"
        bodyPart = "The LLC Data Logger does not receive data anymore from the LLC. Probably the internet cable got loose or the LLC was rebooted."
        helpPart = """Something went wrong with communication. The state of this application is now more or less \
            undefined; there are multiple TCP/IP connections open or closed, some have received error messages, some not etc. \
            I did not attempt to write clean-up code, as it would become extensive. Instead, I'll just suggest to close the \
            application and restart."""
        self._showSevereError(titlePart, bodyPart, helpPart)
            
    def _onTimerTick(self):
        """This function takes care of requesting data from server and writing
        it to the file."""
                 
        # We need to gather all data from different loggers, and then check from which
        # timestamps we have all logging data. Then we have to write all those data
        # to the file.
        N = [C.GetNumberOfSamplesInBuffer() for C in self._C]
        
        if self._isFirst:
            if min(N)<500:
                # We want to be on the way well, so let's get at least 500 samples before doing something
                return
            else:
                # Find out what the oldest timestamp is for which each logger has data
                # and then remove all data from the buffers which is not in all buffers
                oldestTimestamps =  []
                for C in self._C:
                    oldestTimestamps.append(C.GetFirstTimestampInBuffer())
                print("Before syncing:")
                print (oldestTimestamps)
                maxTimeStamps = max(oldestTimestamps)
                for C in self._C:
                    C.RemoveSamplesFromBufferUntilTimestamp(maxTimeStamps)
                    
                print("After syncing:")
                oldestTimestamps =  []
                for C in self._C:
                    oldestTimestamps.append(C.GetFirstTimestampInBuffer())
                print (oldestTimestamps)
                
                # Now all buffers are synchronized.
                self._mostRecentTimeStamp = -1e6 # Just small so that the next sample will be handled correctly
                
                self._isFirst = False
                return # We will store the data in the next sample
               
        # Get all data
        minN=min(N)
        self._watchDogNReceived += minN
            
        for i in range(minN): # For each sample for which all data is already available
            # We do at most minN reads from any channel. It may be that we don't use all available
            # data, but who cares; it will be taken into account the next timer tick then.
        
            # We constantly need to keep track of unsyncing. Therefore, we need, for each sample,
            # to check whether the timestamps are still okay.
            timeStamps = [C.GetFirstTimestampInBuffer() for C in self._C]
            
            minTimeStamp = min(timeStamps)
            
            if any ( [t-minTimeStamp!=0 for t in timeStamps] ):
                if self._mostRecentTimeStamp>self._lastTimeErrorMessage[0]+1:
                    print("Discrepancy: ",timeStamps)
                    self._lastTimeErrorMessage[0] = self._mostRecentTimeStamp
                    
            if minTimeStamp<=self._mostRecentTimeStamp:
                # Then there are time samples in the past. This should not be the case but this
                # happens some times. Just get rid of them
#                 for (i, C, t, d) in zip(range(len(self._C)), self._C, timeStamps, self._debugFiles):
                for (i, C, t) in zip(range(len(self._C)), self._C, timeStamps):
                    if t<=self._mostRecentTimeStamp:
                        if self._mostRecentTimeStamp>self._lastTimeErrorMessage[1]+1:
                            print('LLC data from group %i was in the past, skipping! (group time: %.4f current time: %.4f difference: %g'%(i,t,self._mostRecentTimeStamp,t-self._mostRecentTimeStamp))
                            self._lastTimeErrorMessage[1] = self._mostRecentTimeStamp
                        C.ReadOneFromBuffer()
#                         d.write((str(t)+'\n').encode())

                        
                # We have read data from some buffers, so continue the next iteration (in order to
                # make sure we don't read too often)
                continue
                
            # If we're here, we can create a new sample (which may or may not be saved into a file)
            sample = b''
#             for (i, C, t, d) in zip(range(len(self._C)),self._C, timeStamps, self._debugFiles):
            for (i, C, t) in zip(range(len(self._C)),self._C, timeStamps):
                if t== minTimeStamp:
                    # Then this is a correct sample and we need to use it
                    sample+= C.ReadOneFromBuffer()
#                     d.write((str(t)+'\n').encode())
                else:
                    # Then the next item in the buffer is more in the future than minTimeStamp,
                    # and we need to wait until we are there. Thus, insert dummy data
                    if self._mostRecentTimeStamp>self._lastTimeErrorMessage[2]+1:
                        print('LLC data from group %i is in the future, inserting dummy data!'%i)
                        self._lastTimeErrorMessage[2] = self._mostRecentTimeStamp
                    
                    sample+= C.GenerateDummyData(minTimeStamp)
            
            self._mostRecentTimeStamp = minTimeStamp
                    
            
            # Now, the variable 'sample' contains the full data of one sample time.
            # (possibly with some NaNs if there was missing data)
            
            if self._state==2:
                # Then we're recording, so write the sample to file
                self._file.write(sample)
            
            # Update plot windows: send data to them
            for p in self._plotWindows:
                p.AddOneDataPoint(sample)
                
            # Show limiters action    
            if self._plotLimiters:
                theTime = struct.unpack('<d',sample[8:16])[0]
                
                l = [0]
                for ind in self._limiterChannelNumbers:
                    l.append(struct.unpack('<d',sample[ind*8:(ind+1)*8])[0])
                maxLimit = max(l)    
                if hasattr(self,'_p') and self._p is not None:
                    self._p.points.append([theTime,maxLimit,0.1])
                if i==0:
                    # Update the limiters-text-window
                    def c1(v): return '' if v==0 else '<font color="red">'
                    def c2(v): return '' if v==0 else '</font>'
                    
                    s = '<br>'.join(['%s%d %s%s'%(c1(v),v,n.replace('#Lopes.',''),c2(v)) for v,n in zip(l[1:],self._limiterChannelNames)])
                    self._limitsTextWindow.setHtml(s)
                
        # Update GUI
        if self._state == 2:
            s = self._fileLabelFileText + " - t=%.1f"%(datetime.datetime.now()-self._startTime).total_seconds()
            self._fileLabel.setText (s)
        
        # Update plot windows: redraw
        for p in self._plotWindows:
            p.FinishUpdate()
            
        # Show graph
        if self._plotLimiters and hasattr(self,'_p') and self._p is not None and self._p.GetAxes() is not None:
            TIME_TO_SHOW=4 # How many seconds to show
            self._p.SetPoints(self._p.points[(-TIME_TO_SHOW*self._configuration['sampleFrequency']):])
            self._p.GetAxes().SetLimits()
#             self._p.GetAxes().SetLimits([theTime-TIME_TO_SHOW,theTime],None,None) # time is the latest time, set a few lines up
            self._p.GetAxes().Draw()
          
    def _writeSignalsFileHeader(self, AdditionalFields = None):
        """Writes the header of the signals file to the file. If AdditionalFields
        (a dict) is given, the fields of this dict are added to the header."""
            
        headerComment = """\
            # LOPES SIGNAL DATA LOG
            # File format: SSDF header with a binary body
            # Header fields:
            #     map: list of strings with the names of the signals    
            #     units: list of strings with the unit of each signal
            #     time: string with start time of the log, HHMSS
            #     date: string with start date of the log, YYYYMMDD
            #     sampleTime: sample time of the data in seconds
            # Separator: 
            #     80 '=' characters followed by a linefeed character.
            # Data:
            #     Blob of 64 bit floating point numbers (little endian). Each "row"
            #     of numbers reprsents all signal at one timestamp. Each row contains
            #     N numbers, where N is len(header.map).
            # Finalizer: 
            #     File is finalized with a linefeed character if closed in a nice way.
        """
        headerComment = "\n".join([x.strip() for x in headerComment.split("\n")]) # Strips the leading spaces

        # Prepare AdditionalDict
        if AdditionalFields is None:
            AdditionalFields = {}
            
        fields = {\
        'date': datetime.datetime.strftime(self._startTime, "%Y%m%d"),
        'time': datetime.datetime.strftime(self._startTime, "%H%M%S"),
#         'sampleTime': 1/self._configuration['sampleFrequency'],
        'map': self._mapSimpleNames,
        'units': self._units }
        fields.update(AdditionalFields)
        
        # Write everything. Note that we must ensure that we get \r\n line ending (we can't use automatic
        # conversion because we opened the file in binary mode).
        self._file.write(headerComment.replace('\n','\r\n').encode())
        self._file.write(ssdf.saves(fields).replace('\n','\r\n').encode())
        self._file.write(("\r\n"+80*"="+"\r\n").encode())

## Main program
app = QtGui.QApplication([])    
G =LLCDataLogger(defaultConfiguration)
G.setGeometry(QtCore.QRect(54, 258, 1275, 580))
l = G._channelsList
app.exec_()

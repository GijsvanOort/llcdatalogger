# ChannelSetDefinitions
"""This module creates various lists of channel sets for use in different cases."""

## Joint name lists
jointNames =  ["pelvis tX","pelvis tZ","left hip rX","left hip rZ","left knee rZ","left ankle rZ","right hip rX","right hip rZ","right knee rZ","right ankle rZ"]
jointUnits = ['m','m','rad','rad','rad','rad','rad','rad','rad','rad']
jointUnitsVel = ['%s/s'%u for u in jointUnits]
jointUnitsForce = ['N','N']+8*['Nm']
jointUnitsStiffness =  ['%s/%s'%(u1,u2) for u1,u2 in zip(jointUnitsForce,jointUnits)]
segmentNames = ["pelvis tX","pelvis tZ","left thigh rX","left thigh rZ","left shank rZ","left foot rZ","right thigh rX","right thigh rZ","right shank rZ","right foot rZ","pelvis tY","pelvis ry","pelvis rx"]
segmentUnits = ['m','m','rad','rad','rad','rad','rad','rad','rad','rad','m','rad','rad']
segmentUnitsAcc = ['m/s2','m/s2']+8*['rad/s2']+['m/s2','rad/s2','rad/s2']
segmentForceUnits = ['N','N']+8*['Nm']
segmentAndJointNames = segmentNames + jointNames[2:]
segmentAndJointUnits = segmentUnits + jointUnits[2:]
motorNames = ["MLThighRZ","MRThighRZ","MLThighRX","MRThighRX","MPelvisTX","MPelvisTZ","MLShankRZ","MRShankRZ","MLFootRZ","MRFootRZ"]
## Sets of channels



##


# Model posititons, both segment, joint and segment+joint
JointPosModel = ["Lopes.Model.%s (pos) [%s]"%(s1,s2) for s1,s2 in zip(jointNames,jointUnits)]
SegmentPosModel =  ["Lopes.Model.%s (pos) [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnits)]
FullPosModel = ["Lopes.Model.%s (pos) [%s]"%(n,u) for n,u in zip(segmentAndJointNames, segmentAndJointUnits)]

# Measured positions
SegmentPosMeasMotor = ["Lopes.Patient.Segments.Position.PosMeasMotor%s [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnits)] # Raw as measured by motor encoders
SegmentPosMeas      = ["Lopes.Patient.Segments.Position.Pos meas %s [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnits)] # Best guess (possibly filtered and sensor-fused)

# Measured accelerations
SegmentAccMeasAcc = ["Lopes.Patient.Segments.Acceleration.AccMeasAcc%s [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnitsAcc)] # Raw from acceleration sensors

# Acceleration feed forward
SegmentAccRef = ["Lopes.Model.GlobalAccRefGain [-]"]+["Lopes.Model.%s (acc Ref) [1/s2]"%s for s in segmentNames[:-3]]

# Measured forces
SegmentMeasForceFromRods = ["Lopes.Patient.Segments.Force.%sForceRodsNonLin [%s]"%(s1,s2) for s1,s2 in zip(segmentNames[0:10],segmentForceUnits)]
SegmentForceMeasFused = ["Lopes.Patient.Segments.Force.%sForceFused [%s]"%(s1,s2) for s1,s2 in zip(segmentNames[0:10],segmentForceUnits)] # Best guess (possibly filtered and sensor-fused)

# JointSpring data
JointSpring_pos         = lambda springName: ["Lopes.HapticRenderer.%s.Effect Pos %d [%s]"  %(springName, d+1, s2) for d,s2 in enumerate(jointUnits)]
JointSpring_vel         = lambda springName: ["Lopes.HapticRenderer.%s.Effect Vel %d [%s]"  %(springName, d+1, s2) for d,s2 in enumerate(jointUnitsVel)]
JointSpring_stiffness   = lambda springName: ["Lopes.HapticRenderer.%s.Stiffness%d [%s]"    %(springName, d+1, s2) for d,s2 in enumerate(jointUnitsStiffness)]
JointSpring_outputForce = lambda springName: ["Lopes.HapticRenderer.%s.Output Force %d [%s]"%(springName, d+1, s2) for d,s2 in enumerate(jointUnitsForce)]

# JointSpring data for lopes 3
JointSpring_pos3         = lambda springName: ["Lopes.HapticRenderer.%s.Effect Pos %s [%s]"  %(springName, s1, s2) for s1,s2 in zip(jointNames,jointUnits)]
JointSpring_vel3         = lambda springName: ["Lopes.HapticRenderer.%s.Effect Vel %s [%s]"  %(springName, s1, s2) for s1,s2 in zip(jointNames,jointUnitsVel)]
JointSpring_stiffness3   = lambda springName: ["Lopes.HapticRenderer.%s.Stiffness %s [%s]"    %(springName, s1, s2) for s1,s2 in zip(jointNames,jointUnitsStiffness)]
JointSpring_outputForce3 = lambda springName: ["Lopes.HapticRenderer.%s.Output Force %s [%s]"%(springName, s1, s2) for s1,s2 in zip(jointNames,jointUnitsForce)]

# Limiters (also indicating cause of limit) (3 = for Lopes 3)
MotorLimiters = ["Lopes.Motor PVA Limiters.Motor PVA Limiter %d.Cause of limit int [-]"%(d+1) for d in range(10)]
MotorLimiters3 = ["Lopes.Motor PVA Limiters.%s.Cause of limit int [-]"%s for s in motorNames]
JointLimiters = ["Lopes.Patient.Human joints.%s.Cause of limit int [-]"%s for s in jointNames]

# Analog inputs
AnalogInputs = ["Lopes.Analog user inputs.Analog input %d [V]"%d for d in range(1,17)]

# Various
TreadmillAll = \
"""Lopes.Treadmill.Speed setpoint [m/s]
Lopes.Treadmill.Encoder.VelMeasInternalFiltered [m/s]
Lopes.Treadmill.ForcePlate.CoP_X [m]
Lopes.Treadmill.ForcePlate.CoP_Z [m]
Lopes.Treadmill.ForcePlate.Sum Fy [N]
Lopes.Treadmill.ForcePlate.Sum Fy Filt [N]
Lopes.Treadmill.ForcePlate.Sum Mx [Nm]
Lopes.Treadmill.ForcePlate.Sum Mx Filt [Nm]
Lopes.Treadmill.ForcePlate.Sum Mz [Nm]
Lopes.Treadmill.ForcePlate.Sum Mz Filt [Nm]
""".strip().split('\n')

ShadowLegSensors =\
"""
Lopes.Rod Space.Rod Angles.rod_angle_pelvis_tX (ScaledFiltered) [m]
Lopes.Rod Space.Rod Angles.rod_angle_pelvis_tZ (ScaledFiltered) [m]
Lopes.Rod Space.Rod Angles.rod_angle_left_hip_rX (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_left_hip_rZ (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_left_knee_rZ (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_left_foot_rZ (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_right_hip_rX (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_right_hip_rZ (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_right_knee_rZ (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_right_foot_rZ (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_left_hip_tY (ScaledFiltered) [m]
Lopes.Rod Space.Rod Angles.rod_angle_right_hip_tY (ScaledFiltered) [m]
Lopes.Rod Space.Rod Angles.rod_angle_pelvis_rX (ScaledFiltered) [rad]
Lopes.Rod Space.Rod Angles.rod_angle_pelvis_rY (ScaledFiltered) [rad]
""".strip().split('\n')

RodForceSensors = \
"""
Lopes.Rod Space.Rod Forces.rod_force_left_ankle_X (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_ankle_Y (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_ankle_Z (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_knee_X (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_knee_Y (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_knee_Z (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_hip_X (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_hip_Y (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_left_hip_Z (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_ankle_X (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_ankle_Y (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_ankle_Z (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_knee_X (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_knee_Y (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_knee_Z (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_hip_X (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_hip_Y (Scaled) [N]
Lopes.Rod Space.Rod Forces.rod_force_right_hip_Z (Scaled) [N]
""".strip().split('\n')

State = ["Lopes.State.current state number [-]"]

## Full measurement sets
# For use with development library
LopesDevelopmentLibrary = (
    State +
    FullPosModel +
    SegmentPosMeasMotor +
    SegmentForceMeasFused +
    JointSpring_stiffness('springspring') +
    JointSpring_pos('springspring') +
    JointSpring_outputForce('biasforcespring')+
    TreadmillAll +
    MotorLimiters +
    JointLimiters )

# For use with development library, for use with Lopes3
LopesDevelopmentLibrary3 = (
    State +
    FullPosModel +
    SegmentPosMeasMotor +
    SegmentForceMeasFused +
    JointSpring_pos3('springspring') +
    JointSpring_stiffness3('springspring')+
    JointSpring_outputForce3('biasforcespring')+
    TreadmillAll +
    MotorLimiters3 +
    JointLimiters+
    AnalogInputs +
    RodForceSensors)
    
# LopesGUI
# For use with the therapists LOPES GUI (logs data from JointSpring0)
LopesGUI = \
    FullPosModel + SegmentAccMeasAcc + \
    SegmentPosMeasMotor + \
    SegmentForceMeasFused + \
    MotorLimiters + JointLimiters + \
    SegmentMeasForceFromRods + \
    JointSpring_pos('jointspring0') + JointSpring_stiffness('jointspring0') + \
    ShadowLegSensors + TreadmillAll + State

# LopesGUI
# For use with the therapists LOPES GUI (logs data from JointSpring0), for use with Lopes3
LopesGUI3 = \
    FullPosModel + SegmentAccMeasAcc + \
    SegmentPosMeasMotor + \
    SegmentForceMeasFused + \
    MotorLimiters3 + JointLimiters + \
    SegmentMeasForceFromRods + \
    JointSpring_pos3('jointspring0') + JointSpring_stiffness3('jointspring0') + \
    ShadowLegSensors + TreadmillAll + State
    
# AllChannels
# (not sure why one wants to use this...)
AllChannels = \
SegmentPosModel + SegmentPosMeas + SegmentForceMeasFused + SegmentAccMeasAcc + MotorLimiters + JointLimiters + SegmentPosMeasMotor + \
"""
Lopes.Patient.Segments.Force.pelvis tXForceRodsNonLin [N]
Lopes.Patient.Segments.Force.pelvis tZForceRodsNonLin [N]
Lopes.Patient.Segments.Force.left thigh rXForceRodsNonLin [Nm]
Lopes.Patient.Segments.Force.left thigh rZForceRodsNonLin [Nm]
Lopes.Patient.Segments.Force.left shank rZForceRodsNonLin [Nm]
Lopes.Patient.Segments.Force.left foot rZForceRodsNonLin [Nm]
Lopes.Patient.Segments.Force.right thigh rXForceRodsNonLin [Nm]
Lopes.Patient.Segments.Force.right thigh rZForceRodsNonLin [Nm]
Lopes.Patient.Segments.Force.right shank rZForceRodsNonLin [Nm]
Lopes.Patient.Segments.Force.right foot rZForceRodsNonLin [Nm]
""".strip().split('\n') + \
JointSpring_pos('springspring') + ShadowLegSensors + \
["Lopes.Treadmill.Speed setpoint [m/s]","Lopes.Treadmill.Encoder.VelMeasInternalFiltered [m/s]"]

PositionRedundancyLeftRightKnee = \
"""Lopes.Model.left knee rZ (pos) [rad]
Lopes.Patient.Segments.Position.PosMeasMotorleft shank rZ 
Lopes.Rod Space.Rod Angles.rod_angle_left_knee_rZ (Scaled) [-]
Lopes.Transform.NonLinMeasForward.rod_angle_left_knee_rZCheck [rad]
Lopes.Transform.NonLinMeasForward.rod_angle_left_knee_rZError [rad]
Lopes.Rod Space.Rod Angles.rod_angle_right_knee_rZ (Scaled) [-]
Lopes.Transform.NonLinMeasForward.rod_angle_right_knee_rZCheck [rad]
Lopes.Transform.NonLinMeasForward.rod_angle_right_knee_rZError [rad]
Lopes.Model.right knee rZ (pos) [rad]
Lopes.Patient.Segments.Position.PosMeasMotorright shank rZ""".strip().split('\n')
# The 'Check' is a value calculated from the motor position sensors; it is how much, according to the
# motor position, the shadow leg sensors should be. Thus, the Check and rod_angleXX (Scaled) should be the same.

## Others and generate list
 
ChannelSets = [
    ('Add a set of parameters...', []),
    ('LopesDevelopmentLibrary', LopesDevelopmentLibrary),
    ('LopesDevelopmentLibrary (Lopes 3)', LopesDevelopmentLibrary3),
    ('LopesGUI', LopesGUI),
    ('LopesGUI (Lopes 3)', LopesGUI3),
    ('Measurements140617', AllChannels),
    ('Analog Inputs',AnalogInputs),
    ('JointModelPos', ["Lopes.Model.%s (pos) [%s]"%(s1,s2) for s1,s2 in zip(jointNames,jointUnits)]),
    ('SegmentModelPos', ["Lopes.Model.%s (pos) [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnits)]),
    ('SegmentMeasPos',["Lopes.Patient.Segments.Position.Pos meas %s [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnits)]),
    ('SegmentMeasForce', ["Lopes.Patient.Segments.Force.%sForceFused [%s]"%(s1,s2) for s1,s2 in zip(segmentNames[0:10],segmentForceUnits)]),
    ('SegmentMeasAccFromAccSensors',["Lopes.Patient.Segments.Acceleration.AccMeasAcc%s [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnitsAcc)]),
    ('MotorLimiters', ["Lopes.Motor PVA Limiters.Motor PVA Limiter %d.Was PVA Limited [-]"%(d+1) for d in range(10)]),
    ('JointLimiters', ["Lopes.Patient.Human joints.%s.Cause of limit int [-]"%s for s in jointNames]),
    ('SegmentMeasPosFromMotor',["Lopes.Patient.Segments.Position.PosMeasMotor%s [%s]"%(s1,s2) for s1,s2 in zip(segmentNames,segmentUnits)]),
    ('SegmentMeasForceFromRods', ["Lopes.Patient.Segments.Force.%sForceRodsNonLin [%s]"%(s1,s2) for s1,s2 in zip(segmentNames[0:10],segmentForceUnits)]),
    ('SpringSpring jointPos', ["Lopes.HapticRenderer.springspring.Effect Pos %d [%s]"%(d+1,s2) for d,s2 in enumerate(jointUnits)]),
    ('ShadowLegSensors',ShadowLegSensors),
    ('RodForceSensors',RodForceSensors),
    ('Position redundancy knees', PositionRedundancyLeftRightKnee),
    ('Treadmill speed',["Lopes.Treadmill.Speed setpoint [m/s]","Lopes.Treadmill.Encoder.VelMeasInternalFiltered [m/s]"]),
    ]
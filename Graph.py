# Graph.py
"""This module provides Graph class, which can be used to show real time graphs of data
obtained from the LLC. the class acts as a widget (and it makes most sense to use it as
a separate window). It uses visvis routines for plotting"""
from PySide import QtGui, QtCore
import colorsys
# import visvis as vv
import math
import random
import time
import struct

try:
    import pyqtgraph as pg
except:
    raise ImportError("Failed to import pyqtgraph. Please visit www.pyqtgraph.org and download it.")

# visvisApp = vv.use('pyside')
def colorAsInt(c):
    """Given a color as a tuple (0..1, 0..1, 0..1), returns a list with indices [0..255, 0..255, 0..255]"""
    return [int(i*255) for i in c]
    
class ChannelInfo:
    """This class contains all information about a channel needed for plotting (e.g., its color,
    whether it is plot, scaling). This class also contains a list of all points added to the
    plot (with a maximum history of HISTORY_SIZE_S seconds) and some functions that reduce/decimate
    the number of points for the real plotting (plotting less points is much faster than plotting all of them)."""
    
    HISTORY_SIZE_S = 20 # History size in seconds
    SAMPLE_FREQ = 1024 # Default for LLC
    
    def __init__(self, index, name, unit, show=False, scaling=1, color=(0,0,0)):
        self.index = index # The index in the data structure (e.g., 2 if it is the third signal in the list)
        self.name = name
        self.unit = unit
        self.show = show
        self.scaling = scaling
        self.color = color
        self.lineObject = None
        self._nPointsAdded = 0 # Counts the total number of elements that were added
        self._nPointsRemoved = 0 # Counts the number of elements that were removed again from the points list (because they're too old)
        self._points = []  # List of 3 dimensional points (because the plot command requires 3D points)
        
    def label(self):
        """Returns a string which can be used as a label."""
        scalingString = '' if self.scaling==1 else ('x %d'%self.scaling)
        return '%s [%s] %s'%(self.name, self.unit, scalingString)
        
    def AddPoint(self, p):
        """Given a 2D point p as a tuple, it adds it to the list of points. If needed,
        an element is removed from the front as well. This function takes care of adding
        the third dimension (z=0.1 always).
        This function miserably fails if p is not a tuple (or list) of two elements."""
        while len(self._points)>=self.HISTORY_SIZE_S * self.SAMPLE_FREQ:
            self._points.pop(0)
            self._nPointsRemoved +=1
            
        self._points.append( [p[0],p[1]*self.scaling,0.1] )
        self._nPointsAdded  += 1
        
        # Maybe we can do useful stuff here such as keeping min/max arrays etc.
        # For now, I'll just do decimation
        
    def ClearAllPoints(self):
        self._points = []
        self._nPointsAdded = 0
        self._nPointsRemoved = 0
        
    def GetPointsDecimated(self,decimation):
        """Returns a pointset which is a decimated version of the original pointset.
        In order to avoid jittery, jumpy behaviour, it takes care that it always
        selects the same elements."""
        # May return empty pointset if there are too few points
        return self._points[(decimation-(self._nPointsRemoved%decimation))::decimation]
        
        
## Graph class
class ColoredListWidgetItem(QtGui.QListWidgetItem):
    def __init__(self, *args,**kwargs):
        QtGui.QListWidgetItem.__init__(self, *args, **kwargs)
        self.associatedChannel = None
        
    def setColor(self, c):
        """Given a color as a tuple, it sets the color of this item."""
        self.setForeground( QtGui.QColor.fromRgbF(*c))
        
    def setAssociatedChannel(self,c):
        """Stores a reference to the ChannelInfo associated with this item.
        (makes it easier to manipulate the selected item)"""
        self.associatedChannel = c

class ChannelEditor(QtGui.QDialog):
    """Modal widget to edit channels (on/off, color, ...)"""
    def __init__(self, channels):
        """Given a list of Channel instances, it initializes the widget. Use .Run() to run it."""
        QtGui.QDialog.__init__(self)
            
        self._channels = channels # a list, so it's a reference to the original variable.
        
        self._listWidget = QtGui.QListWidget()
        self._buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok| QtGui.QDialogButtonBox.Cancel)
        self._allButton = QtGui.QPushButton('All')
        self._allButton.clicked.connect(self._selectAll)
        self._noneButton = QtGui.QPushButton('None')
        self._noneButton.clicked.connect(self._selectNone)
        self._buttonBox.accepted.connect(self.accept)
        self._buttonBox.rejected.connect(self.reject)
        
        self._hb = QtGui.QHBoxLayout()
        self._hb.addWidget(self._allButton)
        self._hb.addWidget(self._noneButton)
        self._hb.addStretch()
        self._hb.addWidget(self._buttonBox)
        
        self._vb = QtGui.QVBoxLayout()
        self._vb.addWidget(self._listWidget)
        
        self._vb.addLayout(self._hb)
        self.setLayout(self._vb)
        
        self.setWindowTitle("Edit Channels...")
        self._populateListWidget()
        
    def Run(self):
        """Runs the EditChannel window as modal window. Returns 1 if any changes
        were made to the channel list."""
        def boolFromCheckState(c):
            return True if c is QtCore.Qt.CheckState.Checked else False
            
        result = self.exec_() # Run the window as modal window
        if result == 1:
            # then Ok was pressed, so update the channel list
            for (i,c) in enumerate(self._channels):
                it = self._listWidget.item(i)
                c.show = boolFromCheckState(it.checkState())
                
            return 1
        else:
            return 0
            
    def _removeItemsFromListWidget(self):
        while self._listWidget.count() > 0:
            self._listWidget.takeItem(0)
            
    def _populateListWidget(self):
        def checkState(bool):
            """Returns a checkstate as a function of the bool"""
            return QtCore.Qt.CheckState.Checked if bool else QtCore.Qt.CheckState.Unchecked
            
        self._removeItemsFromListWidget()
        for c in self._channels:
            it = ColoredListWidgetItem(c.label())
            self._listWidget.addItem(it)
            it.setFlags(QtCore.Qt.ItemIsUserCheckable|QtCore.Qt.ItemIsEnabled)
            it.setCheckState(checkState(c.show))
            it.setColor(c.color)
        
    def _selectNone(self):
        for i in range(self._listWidget.count()):
            self._listWidget.item(i).setCheckState(QtCore.Qt.CheckState.Unchecked)
            
    def _selectAll(self):
        for i in range(self._listWidget.count()):
            self._listWidget.item(i).setCheckState(QtCore.Qt.CheckState.Checked)
        
class MyPlotWidget(pg.PlotWidget):
    """Child of pg.PlotWidget, with some changes:
    - Emits a signal when the user clicked on it"""
    leftClicked = QtCore.Signal()
    rightClicked = QtCore.Signal()
    anyClicked = QtCore.Signal()
    
    def __init__(self,*args,**kwargs):
        pg.PlotWidget.__init__(self,*args,**kwargs)
        
    def mousePressEvent(self,ev):
        pg.PlotWidget.mousePressEvent(self,ev)
        
        if ev.button() == QtCore.Qt.MouseButton.LeftButton:
            self.leftClicked.emit()
        elif ev.button()==QtCore.Qt.MouseButton.RightButton:
            self.rightClicked.emit()
        self.anyClicked.emit()
        
class Graph(QtGui.QWidget):
    TIME_TO_SHOW = 20 # How many seconds should be kept in history
    SAMPLE_FREQ = 1024 # = default for LLC
    def __init__(self, map, units, parent = None):
        """Creates the window. 
        parent = parent window (or None if the widget should be a separate window)
        map = list of all channels
        units = list of the unit of each channel"""
        QtGui.QWidget.__init__(self, parent)

        self.decimation = 20
        self.isPaused = False
        
        # Create the graph, make it a widget and add it to the layout
        
#         Figure = visvisApp.GetFigureClass()
#         self._fig = Figure(self)
        self._fig = MyPlotWidget()
        self._fig.setBackground(self.palette().color(QtGui.QPalette.Background).getRgb()[0:3])
        self._fig.anyClicked.connect(lambda: self.PauseResume(True)) # Clicking on axis (for zooming) pauses the plot
        
        # Create rest of the layout
        self._channelList = QtGui.QListWidget()
        self._channelList.setMaximumHeight(400)
        self._channelList.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection) # Allow multiple selections
        self._channelList.itemSelectionChanged.connect(self._onChannelSelectionChanged)
        
        self._pbEditChannels = QtGui.QPushButton('Edit Channels')
        self._pbEditChannels.clicked.connect(self.EditChannels)
        self._pbPauseResume = QtGui.QPushButton('Pause')
        self._pbPauseResume.clicked.connect(self.PauseResume)
        self._vButtonBox = QtGui.QVBoxLayout()
        self._vButtonBox.addWidget(self._pbEditChannels)
        self._vButtonBox.addStretch()
        self._vButtonBox.addWidget(self._pbPauseResume)
        
        self._hb= QtGui.QHBoxLayout()
        self._hb.addWidget(self._channelList)
        self._hb.addLayout(self._vButtonBox)
        self._sp = QtGui.QSplitter(QtCore.Qt.Vertical)
        self._sp.addWidget(self._fig)
        self._hbwidget = QtGui.QWidget()
        self._sp.addWidget(self._hbwidget)
        self._hbwidget.setLayout(self._hb)
        self._bb = QtGui.QVBoxLayout() # Just a container to store the splitter widget in
        self._bb.addWidget(self._sp)
        self.setLayout(self._bb)
        
        # Make the splitter visible
        self._sp.setStyleSheet('QSplitter::handle:vertical {background-color:#888};')
        self._sp.setHandleWidth(3)
        
        self._sp.setStretchFactor(0,22)
        # Populate the channels and show them
        self._populateChannels(map, units)
        self.onChannelsChanged()
        # show the widget
        self.setGeometry(self.pos().x(), self.pos().y(), 800,600)
        self.move(max([self.pos().x(),0]), max([self.pos().y(),0]))  # It usually happens that the title bar is invisible. If so, move it a little
        self.setWindowTitle("LLC Data Logger Graph")
        self._setIcon()
        
    def _setIcon(self):
        self._icon = QtGui.QIcon('LLCDataLoggerGraph.ico')
        self.setWindowIcon(self._icon)
        
    def _populateChannels(self, map, units):
        """Populates the self._channels list"""
        self._channels=[]
        N = len(map)
        for (i,m,u) in zip(range(N), map,units):
            c = colorsys.hsv_to_rgb(i/N, 1,1) # Make evenly distributed colors (0..1, 0..1, 0..1)
            doShow = True if i>=2 and i<7 else False # By default show the first five 'real' elements (skip nSamples and time)
            self._channels.append(ChannelInfo (i,m,u, color = c, show=doShow))
            
    def _onChannelSelectionChanged(self):
        """This is called automatically if the user changes the selection in the
        _channelList widget. In this graph, the behaviour is to show all selected
        channels with thick lines."""
        
        # First set line widths of of all visible lines back to 1
        for c in self._channelsToShow:
            c.lineObject.setPen( colorAsInt(c.color), width=1)
            
        # Now set the line widths of all newly selected to 3
        for it in self._channelList.selectedItems():
            c = it.associatedChannel
            c.lineObject.setPen( colorAsInt(c.color), width=5)
            
        
    def onChannelsChanged(self):
        """This function should be called if any channel info (e.g., which channels
        are shown, the color or scaling of a channel) is changed. It rebuilds the whole plot."""
        self._channelsToShow = [c for c in self._channels if c.show==True]
        
        # Update the channel list
        while self._channelList.count() > 0:
            self._channelList.takeItem(0)
        for c in self._channelsToShow:
            it = ColoredListWidgetItem(c.label())
            it.setAssociatedChannel(c)
            it.setColor(c.color)
            self._channelList.addItem(it)
        
        # Update the plot lines
        for c in self._channels:
            if c.lineObject is None and c.show==True:
                # Then the channel was not shown but should be shown now
                c.lineObject = pg.PlotDataItem() # In the future, I can put my own decimationPlotDataItem in here.
                self._fig.addItem(c.lineObject)
                c.lineObject.setPen(colorAsInt(c.color))
            elif c.lineObject is not None and c.show == False:
                # Then the channel was shown but should be hidden now
                c.ClearAllPoints()
                c.lineObject.clear()
                c.lineObject = None # Remove the references to the lines
                # I guess I should delete the c++-object the lineObject is pointing
                # to as well, but I tried with deleteLater() and that crashes. So
                # let's first try what happens without deletion (and accept memory leakage)
            else:
                # The signal was not shown and should not be shown, or
                # the signal was shown and should still be shown.
                # In both cases, don't do anything.
                pass
        self.PauseResume(toPause=False)
        
    def AddOneDataPoint(self,data):
        """This should be called for each data point"""
        theTime = struct.unpack('<d',data[8:16])[0]
        
        for c in self._channelsToShow:
            value = struct.unpack('<d',data[c.index*8:(c.index+1)*8])[0]
            c.AddPoint([theTime, value] )
            
        self._tmax = theTime
        
    def FinishUpdate(self):
        if not self.isPaused:
            # Show the most recent points
            for c in self._channelsToShow:
                data = c.GetPointsDecimated(self.decimation)
                dataT = list(zip(*data)) # Does a 'transpose'
                c.lineObject.setData(x=list(dataT[0]), y=list(dataT[1]))
#         self._fig.currentAxes.SetLimits([self._tmax-self.TIME_TO_SHOW, self._tmax],[0,50])
            self._fig.autoRange()
#             self._fig.currentAxes.SetLimits()
#         self._fig.currentAxes.Draw()
        else: 
            # Don't do anything; (i.e., don't update the plot)
            pass
    
    def EditChannels(self):
        """Pops up a modal window in which you can enable/disable channels."""
        E = ChannelEditor(self._channels)
        y=E.Run()
        if y:
            self.onChannelsChanged()
        
    def PauseResume(self, toPause = None):
        """Toggle between pause and resume behaviour. If toPause is not None,
        it makes sure that the state is equal to toPause. If toPause is None,
        it toggles the state."""
        if toPause is None:
            toPause = not self.isPaused
        
        if toPause == True:
            # Set to Pause
            self.isPaused = True
            self._pbPauseResume.setText('Resume')
        else:
            # Set to Resume
            self.isPaused = False
            self._pbPauseResume.setText('Pause')

## Test script
     
if __name__ == '__main__':
    # Test for graph class
    M=12 # Number of repetitions of the channels (#channels = 4*M)
    def update():
        t=time.time()-update.t0;
        update.N +=1
        if update.N % 10 == 0:
            print(t, t-update.t10prev, update.N)
            update.t10prev = t
        nSamples = math.floor((t-update.tprev)*1024)
        for i in range(nSamples):
            tt= update.tprev + i/1024
            
            noise = 0.1
            f1 = 1; f2 = 1.1;
            data = (update.N%10,tt, math.sin(tt*2*math.pi*f1)+random.gauss(0,noise), math.sin(tt*2*math.pi*f2)+random.gauss(0,noise))
            databin = struct.pack("<dddd",*data)*M
            G.AddOneDataPoint(databin)
        update.tprev = t
        G.FinishUpdate()
    update.tprev = 0
    update.t10prev= time.time()
    # update.t = 0
    update.t0 = time.time()

    update.N = 0
        
    G = Graph(['no','t','b','c']*M,['-','s','s','m/s']*M)
    G.show()
    T = QtCore.QTimer()
    T.timeout.connect(update)
    T.start(50)
    
    

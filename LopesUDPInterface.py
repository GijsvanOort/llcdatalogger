# Script that asks different strings to the LLC and prints what it gets back
# Also useful: Send(), which sends a command to the llc and prints what it gets back.

# USAGE FOR JOS:
#
# Go to directory in which LopesUDPInterface.py is situated.
# Start python
# >> from LopesUDPInterface import Init, Send
# >> Send('set treadmill 0.1; get treadmill')
# >> Send('dit geeft een foutmelding')
# >> TestScript() # Sequentially sends a various commands to the LLC and shows the return values.

from PySide import QtCore, QtNetwork
import socket
import time, datetime
from pprint import pprint as pp
# import visvis as vv
from math import cos,pi

ip = '10.30.203.225'
#ip='127.0.0.1'

## LopesUDPErrorInterface
class LopesUDPErrorInterface(QtCore.QObject):
    """Interface for receiving error messages from the LLC. When an error message is
    received, a Qt signal, self.ErrorMessageReceived is emitted, containing (timestamp, errorString). The
    timestamp is the one given by datetime.datetime.now() on the local (i.e., this) machine."""
    
    ErrorMessageReceived = QtCore.Signal(datetime.datetime, str)
    
    def __init__(self, isPrint = True):
        QtCore.QObject.__init__(self)
        self._isPrint = isPrint
        # no IP needed for input UDP, only a port, and that's a fixed one
        self._port = 30001
        
        self._sock = QtNetwork.QUdpSocket(self)
        self._sock.bind( self._port )
        self._sock.readyRead.connect(self._process)
    
    def _process(self):
        while self._sock.hasPendingDatagrams():
            tme = datetime.datetime.now()
            
            datagram, host, port = self._sock.readDatagram(self._sock.pendingDatagramSize())
            datagramAsString = datagram.data().decode().strip()
            if self._isPrint:
                tmeString = tme.strftime('%H:%M:%S')
                print("LopesUDPErrorChannel (%s): %s"%(tmeString, datagramAsString))
                
            self.ErrorMessageReceived.emit(tme, datagramAsString)

##  LopesUDPInterface  
    
    
class LopesUDPInterface:
    def __init__(self,ip,isPrint=True, outPort = 25000, inPort =25001, timeout = 2):
        self._ip = ip
        self._isPrint = isPrint
        self._inPort = inPort
        self._outPort = outPort
        self._timeout = timeout
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.bind( ('', self._inPort) )
        self._sock.setblocking(0) # no blocking
    
    def Send(self,data_send,overrideIsPrint=None, overrideTimeout = None):
        if len(data_send.strip())==0:
            return
            
        if overrideIsPrint is not None:
            isPrint = overrideIsPrint
        else:
            isPrint = self._isPrint
        
        timeout = self._timeout if overrideTimeout is None else overrideTimeout # seconds
        
            
        self._sock.sendto(data_send.encode()+'\x00'.encode(),(self._ip,self._outPort))
        
        
        data_recv = None
        t0 = time.time()
        while (time.time()<t0+timeout):
            try:
                (data_recv,addr) = self._sock.recvfrom(10000) # non-blocking; raises error if no data available
                break
            except:
                pass
                
        if data_recv is None:
            if isPrint:
                print (data_send)
                print("timeout")
            else:
                raise Exception("LLC communication timeout")
        else:    
            if isPrint:
                print (data_send)
                print (data_recv.decode())
            else:
                return data_recv.decode()
        if isPrint:
            print(' ')
            
    def Close(self):
        self._sock.close()
    
class Watch:
    def __init__(self, lopesUDPInterface, data_send="get state", fps=2):
        """Sends data_send to the LLC at a frequency of fps times per second. It shows the result on the screen
        in a Label"""
        self._lopesUDPInterface = lopesUDPInterface
        from PySide import QtCore,QtGui
        self._fps = fps
        # Generate GUI
        self.W = QtGui.QWidget()
        self.W.setWindowTitle("LLC Watch")
        self.H=QtGui.QHBoxLayout(self.W)
        self.L = QtGui.QLabel()
        self.L.setStyleSheet('font-size:20pt')
        self.L.currentFontSize=20
        self.L.setWordWrap(True)
        self.H.addWidget(self.L)
    
    
        self.V = QtGui.QVBoxLayout()
        self.E = QtGui.QLineEdit(data_send)
        self.B0 = QtGui.QPushButton("Start")
        self.B1 = QtGui.QPushButton("Stop")
        self.B2 = QtGui.QPushButton("Larger font")
        self.B3 = QtGui.QPushButton("Smaller font")
        self.V.addWidget(self.E)
        self.V.addWidget(self.B0)
        self.V.addWidget(self.B1)
        self.V.addWidget(self.B2)
        self.V.addWidget(self.B3)
        
        
        self.H.addLayout(self.V)
        self.W.setLayout(self.H)
        self.W.show()
        
        # Create Timer
        self.T = QtCore.QTimer()
        self.T.timeout.connect(self.Do)
        
        # Connect GUI button
        self.B0.clicked.connect(self.StartTimer)
        self.B1.clicked.connect(self.StopTimer)
        self.B2.clicked.connect(self.IncreaseFont)
        self.B3.clicked.connect(self.DecreaseFont)
        
        self.StartTimer()
        
    def StartTimer(self):
        self.T.start(1000/self._fps)
        self.B0.setEnabled(False)
        self.B1.setEnabled(True)
        
    def StopTimer(self):
        self.T.stop()
        self.B0.setEnabled(True)
        self.B1.setEnabled(False)
    
    def Do(self):
        y = self._lopesUDPInterface.Send(self.E.text(),False)
        self.L.setText(y.replace(',',', ').replace(';','; '))
    
    
    def IncreaseFont(self):
        self.L.currentFontSize *=1.1
        self.L.setStyleSheet('font-size:%dpt'%self.L.currentFontSize)
    def DecreaseFont(self):
        self.L.currentFontSize /= 1.1
        self.L.setStyleSheet('font-size:%dpt'%self.L.currentFontSize)
    
    
##    
class LogData():
    def __init__(self,lopesUDPInterface,fps=10,query=None):
        self._lopesUDPInterface = lopesUDPInterface
        if query is not None:
            self.query = query
        else:
            # set default
            self.query = "get segmentmeasforce"
            
        self.ms = 1000/fps
        self.T = QtCore.QTimer()
        self.T.timeout.connect(self.LogOne)
    
    def Start(self):
        self.Data = []
        self.DataAsList = None
        print("Logging...")
        self.t0=time.time()
        self.T.start(self.ms)
    def Stop(self):
        self.T.stop()
        self.t_end = self.Data[-1][0]
        print("Stopped logging, postprocessing...")
        print("%f seconds, %d measurements (avg: %f fps)"%(self.t_end, len(self.Data), len(self.Data)/self.t_end))
        self.DataAsList = self.GetLogDataAsList()
        print("Done postprocessing.")
        
        
    def LogOne(self):
        self.Data.append((time.time()-self.t0,self._lopesUDPInterface.Send(self.query,False)))
        
    def GetLogData(self):
        return self.Data
                
    def GetLogDataAsCSV(self):
        return '\n'.join([ ','.join(['%f'%x for x in l]) for l in self.DataAsList])
        
        
    def GetLogDataAsList(self):
        def repl(s):
            return s.replace('[','').replace(']','').replace(';',',')
        d = [ ('%f,%s'%(x[0],repl(x[1]))).split(',') for x in self.Data]
        d2 = [ [float(x) for x in  y] for y in d]
        return d2
        #return [x.split(',') for x in self.GetLogDataAsCSV()]
        
            
    def SaveLogData(self,filename):
        F=open(filename,'w')
        F.write(L.GetLogDataAsCSV())
        F.close()
        
    def LoadLoggedData(self,filename):
        """Reloads a previously saved file"""
        F = open(filename,'r')
        d = [ l.split(',') for l in F.read().split('\n')]
        d2 = [ [float(x) for x in  y] for y in d]
        self.DataAsList = d2
        
    def GetTimes(self):
        return [x[0] for x in self.Data]
                
    def getDt(self):
        t = self.GetTimes()
        return [t[i+1]-t[i] for i in range(len(t)-1)]
        
    def GetColumn(self,i):
        return [x[i] for x in self.DataAsList]

    def Plot(self, indices): 
        """Plots the columns of the list indices as a function of time."""
        from math import floor
        colormap = [[1.0000,0,0],[1.0000,0.0938,0],[1.0000,0.1875,0],[1.0000,0.2813,0],[1.0000,0.3750,0],[1.0000,0.4688,0],[1.0000,0.5625,0],[1.0000,0.6563,0],[1.0000,0.7500,0],
            [1.0000,0.8438,0],[1.0000,0.9375,0],[0.9688,1.0000,0],[0.8750,1.0000,0],[0.7813,1.0000,0],[0.6875,1.0000,0],[0.5938,1.0000,0],[0.5000,1.0000,0],[0.4063,1.0000,0],
            [0.3125,1.0000,0],[0.2188,1.0000,0],[0.1250,1.0000,0],[0.0313,1.0000,0],[0,1.0000,0.0625],[0,1.0000,0.1563],[0,1.0000,0.2500],[0,1.0000,0.3438],[0,1.0000,0.4375],
            [0,1.0000,0.5313],[0,1.0000,0.6250],[0,1.0000,0.7188],[0,1.0000,0.8125],[0,1.0000,0.9063],[0,1.0000,1.0000],[0,0.9063,1.0000],[0,0.8125,1.0000],[0,0.7188,1.0000],
            [0,0.6250,1.0000],[0,0.5313,1.0000],[0,0.4375,1.0000],[0,0.3438,1.0000],[0,0.2500,1.0000],[0,0.1563,1.0000],[0,0.0625,1.0000],[0.0313,0,1.0000],[0.1250,0,1.0000],
            [0.2188,0,1.0000],[0.3125,0,1.0000],[0.4063,0,1.0000],[0.5000,0,1.0000],[0.5938,0,1.0000],[0.6875,0,1.0000],[0.7813,0,1.0000],[0.8750,0,1.0000],[0.9688,0,1.0000],
            [1.0000,0,0.9375],[1.0000,0,0.8438],[1.0000,0,0.7500],[1.0000,0,0.6563],[1.0000,0,0.5625],[1.0000,0,0.4688],[1.0000,0,0.3750],[1.0000,0,0.2813],[1.0000,0,0.1875],
            [1.0000,0,0.0938]]
        colorindices = [floor(64/len(indices)) * x for x in range(len(indices))]
        t = self.GetColumn(0)        
        vv.figure()
        l=[]
        for (n,i) in enumerate(indices):
            d = self.GetColumn(i)
            l.append(vv.plot(t,d))
            l[-1].lc = colormap[colorindices[n]]
            
        vv.legend( ['%d'%i for i in indices])
        vv.xlabel('time [s]')
  
      
##    
    
#pp(Send("get segmentmeasposnonlin;get segmentmeaspos",False).split(";"))        
        
        
##

def TestScript():
    """Sends a few common commands to the LLC and records the answers."""
    
    testscript = """
        resetAll;remove all
        set msgid test;set state off
        set state init
        set state force;get isForceCalibrated
        calibrateforcesensors
        set state force
        set treadmill 0.1
        get treadmill
        set treadmill -0.1
        get inertia
        set inertia [50,50,5,2,1,0.5,5,2,1,0.5]
        set state off
        get segmentModelPos
        get segmentMeasPos
        resetTreadmillDistance 
        create jointspring x
        get patientdim
        set x pos [0,0.0,0.1,1.0e-3,1.0e-2,0.001e1,-0,0,0,0]
        set x pos [1.0e-3,0,0,0,0,0,0,0,0,0]
        set x pos [1.0e6,0,0,0,0,0,0,0,0,0]
        get x pos
        
        # These all fail:
        set x pos [1e3,0,0,0,0,0,0,0,0,0]
        set x pos [1e-3,0,0,0,0,0,0,0,0,0]
        set x pos [1.,0,0,0,0,0,0,0,0,0]
        set x pos [.2,0,0,0,0,0,0,0,0,0]
        
        
        
        #
        # ALLE COMMANDOS VAN HLC
        #
        set msgid 101_1;resetAll
        set msgid 101_2;get isInitOk
        set msgid 107_1;set state init
        set msgid 107_2;get state
        set msgid 107_3;set state off
        set msgid 160_1;set state force
        calibrateforcesensors
        set msgid 160_1;set state force
        set msgid 162_1;set state off
        get state;get isCabinetBlueButtonPressed;get isPatientDefined
        set patientdim [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        set patientdim [1,2,3,4,5,6,7,8,9,10,11,12] ; Will be 12D in the end
        
        #
        # Command in Gait.DataToRobot
        #
        create jointspring jointspring0
        set msgid 1;get segmentmodelpos;get cartesianmodelpos;get treadmill;set jointspring0 pos [0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000];set jointspring0 vel [0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,0.000000,-0.053631,5.293971,2.164673];set jointspring0 stiffness [5.293971,-0.053634,0.000000,2.083130,2.166871,0.000000,0.000000,0.000000,0.000000,2.086426];set treadmill 2.164621
        
        #
        # Commands in LopesDevelopmentInterface
        #
        set msgID 1;get state;get iscabinetbluebuttonpressed;get isinitok;get isMotorsOk;get isTreadmillEmpty;get isPatientDefined;set state position
        set msgID 12;set SpringSpring pos [0.000000,0.000000,0.000000,0.300000,0.000000,0,0.000000,0.000000,0.000000,0];set SpringSpring stiffness [999.000000,999.000000,999.000000,999.000000,999.000000,0,999.000000,999.000000,999.000000,0];set BiasForceSpring stiffness [0,0,0,0,0,0,0,0,0,0]
        set msgID 14;create jointspring BiasForceSpring;set BiasForceSpring maxforce [800,800,80,80,80,80,80,80,80,80];set BiasForceSpring pos [1000,1000,1000,1000,1000,0,1000,1000,1000,0];set BiasForceSpring enable;set SpringSpring pos [0.000000,0.000000,0.000000,0.300000,0.000000,0,0.000000,0.000000,0.000000,0];set SpringSpring stiffness [999.000000,999.000000,999.000000,999.000000,999.000000,0,999.000000,999.000000,999.000000,0];set BiasForceSpring stiffness [0,0,0,0,0,0,0,0,0,0]
        set msgID 15;create jointspring SpringSpring;set SpringSpring maxforce [800,800,80,80,80,80,80,80,80,80];set SpringSpring enable;create jointspring BiasForceSpring;set BiasForceSpring maxforce [800,800,80,80,80,80,80,80,80,80];set BiasForceSpring pos [1000,1000,1000,1000,1000,0,1000,1000,1000,0];set BiasForceSpring enable;set SpringSpring pos [0.000000,0.000000,0.000000,0.300000,0.000000,0,0.000000,0.000000,0.000000,0];set SpringSpring stiffness [999.000000,999.000000,999.000000,999.000000,999.000000,0,999.000000,999.000000,999.000000,0];set BiasForceSpring stiffness [0,0,0,0,0,0,0,0,0,0]
        set msgid 2;get segmentmodelpos;get segmentmodelvel;get segmentmodelacc;get segmentmeaspos;get segmentmeasvel;get segmentmeasacc;get segmentmeasforce;get cartesianmodelpos;get treadmill
        """
        
    # Empty receive buffer
    while True:
        try:
            (data_recv,addr) = sock.recvfrom(10000);
            print("Clearing from input buffer: ",data_recv)
        except:
            break
        
    for x in testscript.split('\n'):
        x=x.lstrip()
        if x.startswith('#') or len(x.strip())==0:
            print(x)
        else:
            L.Send(x)
            time.sleep(0.1)   

 ##       
def loopTreadmill():
    """To start: execute this function. To stop: type T.stop() in the Python shell."""
    f = 0.1;
    vmax = 2;
    t0 = time.time();
    def do():
        # calculate v
        t = time.time()-t0
        v = vmax * (0.5-0.5*cos( t * 2* pi * f))
        L.Send('set treadmill %f'%v,False)
    
    global T
    T = QtCore.QTimer()
    T.timeout.connect(do)
    T.start(10)

## Main (test) script
           
if __name__=='__main__':
    L = LopesUDPInterface(ip)
    Send=lambda x:L.Send(x)
    print("Now, type something like\n  L.Send('get state') or\n  Send('get state')  (= alias) or\n  W=Watch(L,'get state')")
    
    LError=LopesUDPErrorInterface(isPrint=True)     


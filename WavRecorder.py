# WavRecorder.py
from PySide import QtCore, QtMultimedia
import struct

class WavRecorder(QtCore.QObject):
    def __init__(self, filename):
        QtCore.QObject.__init__(self)
        
        # (Hard-coded) settings for the wave file
        self._settings = QtMultimedia.QAudioFormat()
        self._settings.setSampleRate(44100)
        self._settings.setChannelCount(1);
        self._settings.setSampleSize(16);
        self._settings.setCodec("audio/pcm");
        self._settings.setByteOrder(QtMultimedia.QAudioFormat.LittleEndian);
        self._settings.setSampleType(QtMultimedia.QAudioFormat.SignedInt);
        
        self._audio = QtMultimedia.QAudioInput(self._settings)
        
        # Open the file and write the header
        self._f= QtCore.QFile(filename)
        self._f.open(QtCore.QIODevice.WriteOnly)
        self._writeHeader()
        
    def _writeHeader(self):
        """We need to do the header for the wav file ourselves... This
           function expects the file to be self._f and opened for writing."""
        chunksize = 0x10
        self._f.write(b'RIFF')
        self._f.write(b'\0'*4) # Will be filled by _updateDataSize
        self._f.write(b'WAVEfmt ')
        self._f.write(struct.pack('<L',chunksize))
        
        formatTag = 1
        s = self._settings
        data = struct.pack('<HHLLHH',
            0x01, # FormatTag = PCM (?)
            s.channelCount(), 
            s.sampleRate(), 
            s.sampleRate()*s.channelCount()*s.sampleSize()//8,
            0x02, # Block align (?)
            s.sampleSize())
            
        self._f.write(data)
        self._f.write(b'data')
        self._f.write(b'\0'*4) # Will be filled by _updateDataSize
        
        self._fileHeaderSize = self._f.pos()
        
    def _updateDataSize(self):
        """Updates the parts in the header related to the data/file size.
        Assumes that the file pointer is at the end of the file."""
        dataSize = self._f.pos() - self._fileHeaderSize
        riffSize = dataSize + 0x24
        self._f.seek(0x04)
        self._f.write(struct.pack("<L", riffSize))
        self._f.seek(self._fileHeaderSize-4)
        self._f.write(struct.pack("<L", dataSize))
        
    def StartRecording(self):
        """Starts the recording right away. Expects that self._f is an open file
        and that the WAV-header has already been written."""
        self._audio.start(self._f)
        
    def StopRecording(self):
        """Stops recording"""
        self._audio.stop()
        
    def UpdateFileSizeAndCloseFile(self):
        """See function name."""
        self._updateDataSize()
        self._f.close()
        
    def PrintState(self):
        print(self._audio.state())

if __name__=='__main__':
    import time
    w=WavRecorder('d:\\bla.wav')        
    w.StartRecording()
    print("Recording")
    
    def stop():
        w.StopRecording()
        w.UpdateFileSizeAndCloseFile()
        print("Done")

    QtCore.QTimer.singleShot(2000,stop)
    

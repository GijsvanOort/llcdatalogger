import os, platform, subprocess

def ping(host):
    """
    Returns True if host responds to a ping request
    """
    # Ping parameters as function of OS
    if  platform.system().lower()=="windows":
        ping_command = "ping -n 1 -w 1000 "+host  # 1 try, 1000 ms timeout
    else:
        ping_command = "ping -c 1 -W 1 "+host   # 1 try, 1 second timeout
        
    # Ping
    R = subprocess.call(ping_command.split(" "),stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return True if R==0 else False
    

if __name__ == '__main__':
    
    hosts = [ "10.30.203.225", "localhost", "127.0.0.1", "www.google.com", "www.asjeklfsajkejfaske.com" ]
    maxlen = max([len(s) for s in hosts])
    formatString = "%"+str(maxlen)+"s: %s"
    
    print("Checking if hosts are online :")
    for h in hosts:
        print (formatString%(h, ping(h)))
    